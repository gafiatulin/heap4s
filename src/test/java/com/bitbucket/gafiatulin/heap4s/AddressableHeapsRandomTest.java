package com.bitbucket.gafiatulin.heap4s;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Array;
import java.util.*;

import org.junit.Test;

public class AddressableHeapsRandomTest {

    private static final int SIZE = 250000;

    private static final Comparator<Integer> comparator = Comparator.naturalOrder();
    
    @Test
    public void test() {
        test(new Random());
    }

    @Test
    public void testSeed13() {
        test(new Random(13));
    }

    @Test
    public void testSeed37() {
        test(new Random(37));
    }

    @Test
    public void testRandomDeletesSeed37() {
        testRandomDeletes(37);
    }

    @Test
    public void testRandomDelete() {
        testRandomDeletes(new Random().nextLong());
    }

    private void test(Random rng) {

        final int classes = 8;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array.newInstance(AddressableHeap.class,
                classes);
        h[0] = new PairingHeap<>(comparator);
        h[1] = new BinaryTreeHeap<>(comparator);
        h[2] = new FibonacciHeap<>(comparator);
        h[3] = new BinaryArrayHeap<>(comparator);
        h[4] = new CostlessMeldPairingHeap<>(comparator);
        h[5] = new SkewHeap<>(comparator);
        h[6] = new PairingHeap<>(comparator);
        h[7] = new LeftistHeap<>(comparator);

        @SuppressWarnings("unchecked")
        List<AddressableHeap.Handle<Integer, String>>[] s = (List<AddressableHeap.Handle<Integer, String>>[]) Array.newInstance(List.class, classes);
        for (int j = 0; j < classes; j++) {
            s[j] = new ArrayList<>();
        }

        for (int i = 0; i < SIZE; i++) {
            Integer k = rng.nextInt();
            for (int j = 0; j < classes; j++) {
                s[j].add(h[j].insert(k, ""));
            }
            for (int j = 1; j < classes; j++) {
                assertEquals(h[0].findMin().get().getKey().intValue(), h[j].findMin().get().getKey().intValue());
            }
        }

        for (int i = 0; i < 5; i++) {
            @SuppressWarnings("unchecked")
            Iterator<AddressableHeap.Handle<Integer, String>>[] it = (Iterator<AddressableHeap.Handle<Integer, String>>[]) Array.newInstance(Iterator.class,
                    classes);
            for (int j = 0; j < classes; j++) {
                it[j] = s[j].iterator();
            }

            while (true) {
                boolean shouldStop = false;
                for (int j = 0; j < classes; j++) {
                    if (!it[j].hasNext()) {
                        shouldStop = true;
                        break;
                    }
                }

                if (shouldStop) {
                    break;
                }

                @SuppressWarnings("unchecked")
                AddressableHeap.Handle<Integer, String>[] handle = (AddressableHeap.Handle<Integer, String>[]) Array.newInstance(AddressableHeap.Handle.class, classes);
                for (int j = 0; j < classes; j++) {
                    handle[j] = it[j].next();
                }
                int newKey = handle[0].getKey() / 2;
                if (newKey < handle[0].getKey()) {
                    for (int j = 0; j < classes; j++) {
                        handle[j].decreaseKey(newKey);
                    }
                }

                for (int j = 1; j < classes; j++) {
                    assertEquals(h[0].findMin().get().getKey().intValue(), h[j].findMin().get().getKey().intValue());
                }
            }
        }

        while (!h[0].isEmpty()) {
            for (int j = 1; j < classes; j++) {
                assertEquals(h[0].findMin().get().getKey().intValue(), h[j].findMin().get().getKey().intValue());
            }
            for (int j = 0; j < classes; j++) {
                h[j].deleteMin();
            }
        }

    }

    private void testRandomDeletes(long seed) {

        final int classes = 8;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array.newInstance(AddressableHeap.class,
                classes);
        h[0] = new PairingHeap<>(comparator);
        h[1] = new BinaryTreeHeap<>(comparator);
        h[2] = new FibonacciHeap<>(comparator);
        h[3] = new BinaryArrayHeap<>(comparator);
        h[4] = new CostlessMeldPairingHeap<>(comparator);
        h[5] = new SkewHeap<>(comparator);
        h[6] = new PairingHeap<>(comparator);
        h[7] = new LeftistHeap<>(comparator);

        @SuppressWarnings("unchecked")
        List<AddressableHeap.Handle<Integer, String>>[] s = (List<AddressableHeap.Handle<Integer, String>>[]) Array.newInstance(List.class, classes);
        for (int i = 0; i < classes; i++) {
            s[i] = new ArrayList<>();
        }

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < classes; j++) {
                s[j].add(h[j].insert(i, ""));
            }
            for (int j = 1; j < classes; j++) {
                assertEquals(h[0].findMin().get().getKey().intValue(), h[j].findMin().get().getKey().intValue());
            }
        }

        for (int j = 0; j < classes; j++) {
            Collections.shuffle(s[j], new Random(seed));
        }

        for (int i = 0; i < SIZE; i++) {
            for (int j = 1; j < classes; j++) {
                assertEquals(h[0].findMin().get().getKey().intValue(), h[j].findMin().get().getKey().intValue());
            }
            for (int j = 0; j < classes; j++) {
                s[j].get(i).delete();
            }
        }

        for (int j = 0; j < classes; j++) {
            assertTrue(h[j].isEmpty());
        }

    }

}