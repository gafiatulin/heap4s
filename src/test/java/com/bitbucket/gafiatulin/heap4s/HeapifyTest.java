package com.bitbucket.gafiatulin.heap4s;

import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Random;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class HeapifyTest {
    private static final int SIZE = 100000;

    private static Comparator<Integer> comparator;

    @BeforeClass
    public static void setUpClass() {
        comparator = (o1, o2) -> {
            if (o1 < o2) {
                return 1;
            } else if (o1 > o2) {
                return -1;
            } else {
                return 0;
            }
        };
    }

    @Test
    public void testHeapifySort() {
        Random generator = new Random(1);

        final int classes = 5;

        Integer[] a = new Integer[SIZE];
        String[] b = new String[SIZE];
        for (int i = 0; i < SIZE; i++) {
            a[i] = generator.nextInt();
            b[i] = "";
        }
        
        

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array.newInstance(AddressableHeap.class, classes);

        h[0] = BinaryArrayHeap.heapify(a, b, Comparator.naturalOrder());
        h[1] = DaryArrayHeap.heapify(2, a, b, Comparator.naturalOrder());
        h[2] = DaryArrayHeap.heapify(3, a, b, Comparator.naturalOrder());
        h[3] = DaryArrayHeap.heapify(4, a, b, Comparator.naturalOrder());
        h[4] = DaryArrayHeap.heapify(5, a, b, Comparator.naturalOrder());

        int elements = SIZE;
        Integer prev = null, cur;
        while (elements > 0) {
            cur = h[0].findMin().get().getKey();
            for (int i = 1; i < classes; i++) {
                assertEquals(cur.intValue(), h[i].findMin().get().getKey().intValue());
            }
            for (int i = 0; i < classes; i++) {
                h[i].deleteMin();
            }
            if (prev != null) {
                assertTrue(prev.compareTo(cur) <= 0);
            }
            prev = cur;
            elements--;
        }
    }

    @Test
    public void testHeapifySortWithComparator() {
        Random generator = new Random(1);

        final int classes = 5;

        Integer[] a = new Integer[SIZE];
        String[] b = new String[SIZE];
        for (int i = 0; i < SIZE; i++) {
            a[i] = generator.nextInt();
            b[i] = "";
        }

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array.newInstance(AddressableHeap.class, classes);

        h[0] = BinaryArrayHeap.heapify(a, b, comparator);
        h[1] = DaryArrayHeap.heapify(2, a, b, comparator);
        h[2] = DaryArrayHeap.heapify(3, a, b, comparator);
        h[3] = DaryArrayHeap.heapify(4, a, b, comparator);
        h[4] = DaryArrayHeap.heapify(5, a, b, comparator);

        int elements = SIZE;
        Integer prev = null, cur;
        while (elements > 0) {
            cur = h[0].findMin().get().getKey();
            for (int i = 1; i < classes; i++) {
                assertEquals(cur.intValue(), h[i].findMin().get().getKey().intValue());
            }
            for (int i = 0; i < classes; i++) {
                h[i].deleteMin();
            }
            if (prev != null) {
                assertTrue(comparator.compare(prev, cur) <= 0);
            }
            prev = cur;
            elements--;
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHeapifyZeroLengthArray() {
        Integer[] a = new Integer[0];
        String[] b = new String[0];
        final int nonfixed = 5;

        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array.newInstance(AddressableHeap.class, nonfixed);

        h[0] = BinaryArrayHeap.heapify(a, b, Comparator.naturalOrder());
        h[1] = DaryArrayHeap.heapify(2, a, b, Comparator.naturalOrder());
        h[2] = DaryArrayHeap.heapify(3, a, b, Comparator.naturalOrder());
        h[3] = DaryArrayHeap.heapify(4, a, b, Comparator.naturalOrder());
        h[4] = DaryArrayHeap.heapify(5, a, b, Comparator.naturalOrder());

        for (int i = 0; i < nonfixed; i++) {
            assertTrue(h[i].isEmpty());
            try {
                h[i].insert(1, "");
            } catch (IllegalStateException e) {
                fail("No!");
            }
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHeapifyZeroLengthArrayComparator() {
        Integer[] a = new Integer[0];
        String[] b = new String[0];
        final int nonfixed = 5;

        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array.newInstance(AddressableHeap.class, nonfixed);

        h[0] = BinaryArrayHeap.heapify(a, b, comparator);
        h[1] = DaryArrayHeap.heapify(2, a, b, comparator);
        h[2] = DaryArrayHeap.heapify(3, a, b, comparator);
        h[3] = DaryArrayHeap.heapify(4, a, b, comparator);
        h[4] = DaryArrayHeap.heapify(5, a, b, comparator);

        for (int i = 0; i < nonfixed; i++) {
            assertTrue(h[i].isEmpty());
            try {
                h[i].insert(1, "");
            } catch (IllegalStateException e) {
                fail("No!");
            }
        }
    }

    @Test
    public void testHeapifyZeroLengthArray1() {
        Integer[] a = new Integer[0];
        String[] b = new String[0];
        final int classes = 4;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array
                .newInstance(AddressableHeap.class, classes);

        h[0] = BinaryArrayHeap.heapify(a, b, Comparator.naturalOrder());
        h[1] = DaryArrayHeap.heapify(3, a, b, Comparator.naturalOrder());
        h[2] = DaryArrayHeap.heapify(4, a, b, Comparator.naturalOrder());
        h[3] = DaryArrayHeap.heapify(5, a, b, Comparator.naturalOrder());

        for (int i = 0; i < classes; i++) {
            assertTrue(h[i].isEmpty());
            try {
                assertEquals(1, h[i].insert(1, "").getKey().intValue());
            } catch (IllegalStateException e) {
                fail("No!");
            }
        }
    }

    @Test
    public void testHeapifyZeroLengthArray2() {
        Integer[] a = new Integer[0];
        String[] b = new String[0];
        final int classes = 4;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array
                .newInstance(AddressableHeap.class, classes);
        h[0] = BinaryArrayHeap.heapify(a, b, Comparator.naturalOrder());
        h[1] = DaryArrayHeap.heapify(3, a,  b, Comparator.naturalOrder());
        h[2] = DaryArrayHeap.heapify(4, a,  b, Comparator.naturalOrder());
        h[3] = DaryArrayHeap.heapify(5, a,  b, Comparator.naturalOrder());

        for (int i = 0; i < classes; i++) {
            h[i].insert(1, "");
            h[i].insert(2, "");
            h[i].insert(3, "");
            h[i].insert(4, "");
            assertEquals(4, h[i].size());
        }
    }

    @Test
    public void testHeapifyZeroLengthArrayComparator1() {
        Integer[] a = new Integer[0];
        String[] b = new String[0];
        final int classes = 4;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array
                .newInstance(AddressableHeap.class, classes);
        h[0] = BinaryArrayHeap.heapify(a,  b, Comparator.naturalOrder());
        h[1] = DaryArrayHeap.heapify(3, a,  b, Comparator.naturalOrder());
        h[2] = DaryArrayHeap.heapify(4, a,  b, Comparator.naturalOrder());
        h[3] = DaryArrayHeap.heapify(5, a,  b, Comparator.naturalOrder());

        for (int i = 0; i < classes; i++) {
            assertTrue(h[i].isEmpty());
            try {
                assertEquals(1, h[i].insert(1, "").getKey().intValue());
            } catch (IllegalStateException e) {
                fail("No!");
            }
        }
    }

    @Test
    public void testHeapifyBadParameters() {
        Integer[] a = new Integer[0];

        try {
            BinaryArrayHeap.heapify(null, null, Comparator.naturalOrder());
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            BinaryArrayHeap.heapify(null, comparator);
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            DaryArrayHeap.heapify(1, a, null, null);
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            DaryArrayHeap.heapify(1, a, null, comparator);
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            DaryArrayHeap.heapify(2, null, null, null);
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            DaryArrayHeap.heapify(2, null, null, comparator);
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            BinaryArrayHeap.heapify(null, null);
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            BinaryArrayHeap.heapify(null, null, comparator);
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            BinaryArrayHeap.heapify(new Integer[2], new Integer[3], Comparator.naturalOrder());
            fail("No!");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            BinaryArrayHeap.heapify(new Integer[2], new Integer[3], comparator);
            fail("No!");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            DaryArrayHeap.heapify(1, new Integer[2], new Integer[2], Comparator.naturalOrder());
            fail("No!");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            DaryArrayHeap.heapify(3, null, new Integer[2], Comparator.naturalOrder());
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            DaryArrayHeap.heapify(3, new Integer[3], new Integer[2], Comparator.naturalOrder());
            fail("No!");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            DaryArrayHeap.heapify(1, new Integer[2], new Integer[2], comparator);
            fail("No!");
        } catch (IllegalArgumentException ignored) {
        }

        try {
            DaryArrayHeap.heapify(3, null, new Integer[2], comparator);
            fail("No!");
        } catch (NullPointerException ignored) {
        }

        try {
            DaryArrayHeap.heapify(3, new Integer[3], new Integer[2], comparator);
            fail("No!");
        } catch (IllegalArgumentException ignored) {
        }

    }

    @Test
    public void testArrayAddressableHeapifySort() {
        Random generator = new Random(1);

        Integer[] a = new Integer[SIZE];
        String[] b = new String[SIZE];
        for (int i = 0; i < SIZE; i++) {
            a[i] = generator.nextInt();
            b[i] = "";
        }

        int classes = 4;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array
                .newInstance(AddressableHeap.class, classes);
        h[0] = BinaryArrayHeap.heapify(a, b, Comparator.naturalOrder());
        h[1] = DaryArrayHeap.heapify(3, a, b, Comparator.naturalOrder());
        h[2] = DaryArrayHeap.heapify(4, a, b, Comparator.naturalOrder());
        h[3] = DaryArrayHeap.heapify(5, a, b, Comparator.naturalOrder());

        int elements = SIZE;
        Integer[] prev = new Integer[classes];
        Integer[] cur = new Integer[classes];
        while (elements > 0) {
            for (int i = 0; i < classes; i++) {
                cur[i] = h[i].findMin().get().getKey();
                if (i > 0) {
                    assertEquals(cur[0], cur[i]);
                }
            }
            for (int i = 0; i < classes; i++) {
                h[i].deleteMin();
            }
            for (int i = 0; i < classes; i++) {
                if (prev[i] != null) {
                    assertTrue(prev[i].compareTo(cur[i]) <= 0);
                }
                prev[i] = cur[i];
            }
            elements--;
        }
    }

    @Test
    public void testArrayAddressableHeapifySortWithValues() {
        Random generator = new Random(1);

        Integer[] a = new Integer[SIZE];
        String[] b = new String[SIZE];
        for (int i = 0; i < SIZE; i++) {
            a[i] = generator.nextInt();
            b[i] = a[i].toString();
        }

        int classes = 4;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array
                .newInstance(AddressableHeap.class, classes);
        h[0] = BinaryArrayHeap.heapify(a, b, Comparator.naturalOrder());
        h[1] = DaryArrayHeap.heapify(3, a, b, Comparator.naturalOrder());
        h[2] = DaryArrayHeap.heapify(4, a, b, Comparator.naturalOrder());
        h[3] = DaryArrayHeap.heapify(5, a, b, Comparator.naturalOrder());

        int elements = SIZE;
        Integer[] prev = new Integer[classes];
        Integer[] cur = new Integer[classes];
        while (elements > 0) {
            for (int i = 0; i < classes; i++) {
                cur[i] = h[i].findMin().get().getKey();
                assertEquals(h[i].findMin().get().getValue(), h[i].findMin().get().getKey().toString());
                if (i > 0) {
                    assertEquals(cur[0], cur[i]);
                }
            }
            for (int i = 0; i < classes; i++) {
                h[i].deleteMin();
            }
            for (int i = 0; i < classes; i++) {
                if (prev[i] != null) {
                    assertTrue(prev[i].compareTo(cur[i]) <= 0);
                }
                prev[i] = cur[i];
            }
            elements--;
        }
    }

    @Test
    public void testArrayAddressableHeapifySortComparator() {
        Random generator = new Random(1);

        Integer[] a = new Integer[SIZE];
        String[] b = new String[SIZE];
        for (int i = 0; i < SIZE; i++) {
            a[i] = generator.nextInt();
            b[i] = "";
        }

        int classes = 4;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array
                .newInstance(AddressableHeap.class, classes);
        h[0] = BinaryArrayHeap.heapify(a, b, comparator);
        h[1] = DaryArrayHeap.heapify(3, a, b, comparator);
        h[2] = DaryArrayHeap.heapify(4, a, b, comparator);
        h[3] = DaryArrayHeap.heapify(5, a, b, comparator);

        int elements = SIZE;
        Integer[] prev = new Integer[classes];
        Integer[] cur = new Integer[classes];
        while (elements > 0) {
            for (int i = 0; i < classes; i++) {
                cur[i] = h[i].findMin().get().getKey();
                if (i > 0) {
                    assertEquals(cur[0], cur[i]);
                }
            }
            for (int i = 0; i < classes; i++) {
                h[i].deleteMin();
            }
            for (int i = 0; i < classes; i++) {
                if (prev[i] != null) {
                    assertTrue(comparator.compare(prev[i], cur[i]) <= 0);
                }
                prev[i] = cur[i];
            }
            elements--;
        }
    }

    @Test
    public void testArrayAddressableHeapifySortComparatorWithValues() {
        Random generator = new Random(1);

        Integer[] a = new Integer[SIZE];
        String[] b = new String[SIZE];
        for (int i = 0; i < SIZE; i++) {
            a[i] = generator.nextInt();
            b[i] = a[i].toString();
        }

        int classes = 4;

        @SuppressWarnings("unchecked")
        AddressableHeap<Integer, String>[] h = (AddressableHeap<Integer, String>[]) Array
                .newInstance(AddressableHeap.class, classes);
        h[0] = BinaryArrayHeap.heapify(a, b, comparator);
        h[1] = DaryArrayHeap.heapify(3, a, b, comparator);
        h[2] = DaryArrayHeap.heapify(4, a, b, comparator);
        h[3] = DaryArrayHeap.heapify(5, a, b, comparator);

        int elements = SIZE;
        Integer[] prev = new Integer[classes];
        Integer[] cur = new Integer[classes];
        while (elements > 0) {
            for (int i = 0; i < classes; i++) {
                cur[i] = h[i].findMin().get().getKey();
                assertEquals(h[i].findMin().get().getValue(), h[i].findMin().get().getKey().toString());
                if (i > 0) {
                    assertEquals(cur[0], cur[i]);
                }
            }
            for (int i = 0; i < classes; i++) {
                h[i].deleteMin();
            }
            for (int i = 0; i < classes; i++) {
                if (prev[i] != null) {
                    assertTrue(comparator.compare(prev[i], cur[i]) <= 0);
                }
                prev[i] = cur[i];
            }
            elements--;
        }
    }
}
