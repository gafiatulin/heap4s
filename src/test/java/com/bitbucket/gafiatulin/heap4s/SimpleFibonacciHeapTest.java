package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class SimpleFibonacciHeapTest extends AddressableHeapTest  {
    @Override
    protected AddressableHeap<Integer, String> createHeap() {
        return new SimpleFibonacciHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new SimpleFibonacciHeap<>(comparator);
    }

    @Override
    protected AddressableHeap<Integer, String> createHeapWithStringValues() {
        return new SimpleFibonacciHeap<>(Comparator.naturalOrder());
    }
}
