package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class FibonacciHeapTest extends AddressableHeapTest  {
    @Override
    protected AddressableHeap<Integer, String> createHeap() {
        return new FibonacciHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new FibonacciHeap<>(comparator);
    }

    @Override
    protected AddressableHeap<Integer, String> createHeapWithStringValues() {
        return new FibonacciHeap<>(Comparator.naturalOrder());
    }
}
