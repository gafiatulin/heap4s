package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class PairingHeapMergeTest extends MergeableHeapTest {
    protected MergeableHeap<Integer, String> createHeap() {
        return new PairingHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected MergeableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new PairingHeap<>(comparator);
    }
}
