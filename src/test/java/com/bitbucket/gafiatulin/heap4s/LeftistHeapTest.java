package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class LeftistHeapTest extends AddressableHeapTest {
    @Override
    protected AddressableHeap<Integer, String> createHeap() {
        return new LeftistHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new LeftistHeap<>(comparator);
    }

    @Override
    protected AddressableHeap<Integer, String> createHeapWithStringValues() {
        return new LeftistHeap<>(Comparator.naturalOrder());
    }
}
