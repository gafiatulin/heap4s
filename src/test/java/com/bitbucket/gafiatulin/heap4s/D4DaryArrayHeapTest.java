package com.bitbucket.gafiatulin.heap4s;

import org.junit.Test;

import java.util.Comparator;

public class D4DaryArrayHeapTest extends AddressableHeapTest {
    @Override
    protected AddressableHeap<Integer, String> createHeap() {
        return new DaryArrayHeap<>(4, Comparator.naturalOrder());
    }

    @Override
    protected AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new DaryArrayHeap<>(4, comparator);
    }

    @Override
    protected AddressableHeap<Integer, String> createHeapWithStringValues() {
        return new DaryArrayHeap<>(4, Comparator.naturalOrder());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegal1() {
        new DaryArrayHeap<Integer, String>(4, Comparator.naturalOrder(), -5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegal2() {
        new DaryArrayHeap<Integer, String>(4,Comparator.naturalOrder(), Integer.MAX_VALUE);
    }
}
