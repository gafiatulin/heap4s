package com.bitbucket.gafiatulin.heap4s;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class BinaryTreeSoftHeapTest {
    private static final int SIZE = 100000;

    private static Comparator<Integer> comparator;

    @BeforeClass
    public static void setUpClass() {
        comparator = (o1, o2) -> {
            if (o1 < o2) {
                return 1;
            } else if (o1 > o2) {
                return -1;
            } else {
                return 0;
            }
        };
    }

    @Test
    public void testNoCorruptionAsHeap() {
        final int n = SIZE;
        double epsilon = 1.0 / (n + 1);

        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(epsilon, Comparator.naturalOrder());

        for (int i = 0; i < n; i++) {
            a.insert(i, "");
        }

        for (int i = 0; i < n; i++) {
            assertEquals(i, a.deleteMin().get().getKey().intValue());
        }

        assertTrue(a.isEmpty());
    }

    @Test
    public void testNoCorruptionWithComparatorAsHeap() {
        final int n = SIZE;
        double epsilon = 1.0 / (n + 1);

        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(epsilon,
                comparator);

        for (int i = 0; i < n; i++) {
            a.insert(i, "");
        }

        for (int i = SIZE - 1; i >= 0; i--) {
            assertEquals(i, a.deleteMin().get().getKey().intValue());
        }

        assertTrue(a.isEmpty());
    }

    @Test
    public void testSort1RandomSeed1() {
        AddressableHeap<Integer, String> h = new BinaryTreeSoftHeap<>(1.0 / (SIZE + 1), Comparator.naturalOrder());

        Random generator = new Random(1);

        for (int i = 0; i < SIZE; i++) {
            h.insert(generator.nextInt(), "");
        }

        Integer prev = null, cur;
        while (!h.isEmpty()) {
            cur = h.deleteMin().get().getKey();
            if (prev != null) {
                assertTrue(prev.compareTo(cur) <= 0);
            }
            prev = cur;
        }
    }

    @Test
    public void testSort1RandomSeed1WithComparator() {
        AddressableHeap<Integer, String> h = new BinaryTreeSoftHeap<>(1.0 / (SIZE + 1),
                comparator);

        Random generator = new Random(1);

        for (int i = 0; i < SIZE; i++) {
            h.insert(generator.nextInt(), "");
        }

        Integer prev = null, cur;
        while (!h.isEmpty()) {
            cur = h.deleteMin().get().getKey();
            if (prev != null) {
                assertTrue(prev.compareTo(cur) >= 0);
            }
            prev = cur;
        }
    }

    @Test
    public void testFindMinDeleteMinSameObject() {
        AddressableHeap<Long, String> h = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());

        Random generator = new Random(1);

        for (int i = 0; i < SIZE; i++) {
            h.insert(generator.nextLong(), "");
        }

        while (!h.isEmpty()) {
            assertEquals(h.findMin(), h.deleteMin());
        }
    }

    @Test
    public void testSoftHeap0() {
        testSoftHeapInsert(0.01, Comparator.naturalOrder());
        testSoftHeapInsert(0.01, comparator);
        testSoftHeapInsertDeleteMin(0.01, Comparator.naturalOrder());
        testSoftHeapInsertDeleteMin(0.01, comparator);
        testSoftHeapInsertDelete(0.01, Comparator.naturalOrder());
        testSoftHeapInsertDelete(0.01, comparator);
        testSoftHeapInsertDeleteDeleteMin(0.01, Comparator.naturalOrder());
        testSoftHeapInsertDeleteDeleteMin(0.01, comparator);
    }

    @Test
    public void testSoftHeap1() {
        testSoftHeapInsert(0.25, Comparator.naturalOrder());
        testSoftHeapInsert(0.25, comparator);
        testSoftHeapInsertDeleteMin(0.25, Comparator.naturalOrder());
        testSoftHeapInsertDeleteMin(0.25, comparator);
        testSoftHeapInsertDelete(0.25, Comparator.naturalOrder());
        testSoftHeapInsertDelete(0.25, comparator);
        testSoftHeapInsertDeleteDeleteMin(0.25, Comparator.naturalOrder());
        testSoftHeapInsertDeleteDeleteMin(0.25, comparator);
    }

    @Test
    public void testSoftHeap2() {
        testSoftHeapInsert(0.5, Comparator.naturalOrder());
        testSoftHeapInsert(0.5, comparator);
        testSoftHeapInsertDeleteMin(0.5, Comparator.naturalOrder());
        testSoftHeapInsertDeleteMin(0.5, comparator);
        testSoftHeapInsertDelete(0.5, Comparator.naturalOrder());
        testSoftHeapInsertDelete(0.5, comparator);
        testSoftHeapInsertDeleteDeleteMin(0.5, Comparator.naturalOrder());
        testSoftHeapInsertDeleteDeleteMin(0.5, comparator);
    }

    @Test
    public void testSoftHeap3() {
        testSoftHeapInsert(0.75, Comparator.naturalOrder());
        testSoftHeapInsert(0.75, comparator);
        testSoftHeapInsertDeleteMin(0.75, Comparator.naturalOrder());
        testSoftHeapInsertDeleteMin(0.75, comparator);
        testSoftHeapInsertDelete(0.75, Comparator.naturalOrder());
        testSoftHeapInsertDelete(0.75, comparator);
        testSoftHeapInsertDeleteDeleteMin(0.75, Comparator.naturalOrder());
        testSoftHeapInsertDeleteDeleteMin(0.75, comparator);
    }

    @Test
    public void testSoftHeap4() {
        testSoftHeapInsert(0.99, Comparator.naturalOrder());
        testSoftHeapInsert(0.99, comparator);
        testSoftHeapInsertDeleteMin(0.99, Comparator.naturalOrder());
        testSoftHeapInsertDeleteMin(0.99, comparator);
        testSoftHeapInsertDelete(0.99, Comparator.naturalOrder());
        testSoftHeapInsertDelete(0.99, comparator);
        testSoftHeapInsertDeleteDeleteMin(0.99, Comparator.naturalOrder());
        testSoftHeapInsertDeleteDeleteMin(0.99, comparator);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalConstruction1() {
        new BinaryTreeSoftHeap<Integer, String>(0.0, Comparator.naturalOrder());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalConstruction2() {
        new BinaryTreeSoftHeap<Integer, String>(1.0, Comparator.naturalOrder());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalConstruction3() {
        new BinaryTreeSoftHeap<Integer, String>(-1.0, Comparator.naturalOrder());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalConstruction4() {
        new BinaryTreeSoftHeap<Integer, String>(2.0, Comparator.naturalOrder());
    }

    @Test
    public void testIllegalDeleteMin() {
        assertTrue(new BinaryTreeSoftHeap<Integer, String>(0.5, Comparator.naturalOrder()).deleteMin().isEmpty());
    }

    @Test
    public void testIllegalFindMin() {
        assertTrue(new BinaryTreeSoftHeap<Integer, String>(0.5, Comparator.naturalOrder()).findMin().isEmpty());
    }

    @Test
    public void testIllegalDeleteTwice() {
        BinaryTreeSoftHeap<Integer, String> h = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        AddressableHeap.Handle<Integer, String> e = h.insert(1, "");
        assertTrue(e.delete());
        assertFalse(e.delete());
    }

    @Test
    public void testIterator(){
        Random generator = new Random(1);
        int[] ref = new int[SIZE];
        AddressableHeap<Integer, String> h = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        for (int i = 0; i < SIZE; i++) {
            String v = String.valueOf(i);
            int key = generator.nextInt();
            h.insert(key, v);
            ref[i] = key;
        }
        int[] result = new int[SIZE];
        Iterator<AddressableHeap.Handle<Integer, String>> iterator = h.iterator();
        int i = 0;
        while(iterator.hasNext()){
            result[i] = iterator.next().getKey();
            i++;
        }
        assertEquals(i, h.size());
        Arrays.sort(result);
        Arrays.sort(ref);
        assertArrayEquals(ref, result);
        assertFalse(new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder()).iterator().hasNext());
    }

    @Test
    public void testGetValue() {
        AddressableHeap<Integer, String> h = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        AddressableHeap.Handle<Integer, String> e = h.insert(1, "999");
        assertEquals("999", e.getValue());
    }

    @Test
    public void testNoDecreaseKey() {
        AddressableHeap<Integer, String> h = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        AddressableHeap.Handle<Integer, String> e = h.insert(1, "999");
        assertFalse(e.decreaseKey(0));
    }

    @Test
    public void testIsEmpty() {
        AddressableHeap<Integer, String> h = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        assertTrue(h.isEmpty());
        h.insert(1, "");
        assertFalse(h.isEmpty());
    }

    @Test
    public void testClear() {
        AddressableHeap<Integer, String> h = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        assertTrue(h.isEmpty());
        h.insert(1, "");
        h.insert(2, "");
        h.insert(3, "");
        h.insert(4, "");
        assertFalse(h.isEmpty());
        assertEquals(4, h.size());
        h.clear();
        assertTrue(h.isEmpty());
        assertEquals(0, h.size());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSerializable() throws IOException, ClassNotFoundException {
        AddressableHeap<Long, String> h = new BinaryTreeSoftHeap<>(1.0 / 16, Comparator.naturalOrder());

        for (long i = 0; i < 15; i++) {
            h.insert(i, "");
        }

        // write
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(h);
        oos.close();
        byte[] data = baos.toByteArray();

        // read

        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o = ois.readObject();
        ois.close();
        h = (AddressableHeap<Long, String>) o;

        for (int i = 0; i < 15; i++) {
            assertEquals(15 - i, h.size());
            assertEquals(Long.valueOf(i), h.findMin().get().getKey());
            h.deleteMin();
        }
        assertTrue(h.isEmpty());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testMeldGeneric() {
        AddressableHeap<Integer, String> h1 = new BinaryTreeSoftHeap<>(1.0 / (SIZE + 1), Comparator.naturalOrder());

        if (h1 instanceof MergeableHeap) {
            for (int i = 0; i < SIZE; i++) {
                h1.insert(2 * i, "");
            }

            AddressableHeap<Integer, String> h2 = new BinaryTreeSoftHeap<>(1.0 / (SIZE + 1), Comparator.naturalOrder());
            for (int i = 0; i < SIZE; i++) {
                h2.insert(2 * i + 1, "");
            }

            ((MergeableHeap<Integer, String>) h1).meld((MergeableHeap<Integer, String>) h2);

            assertEquals(h1.size(), SIZE * 2);
            assertEquals(h2.size(), 0);

            Integer prev = null, cur;
            while (!h1.isEmpty()) {
                cur = h1.findMin().get().getKey();
                h1.deleteMin();
                if (prev != null) {
                    assertTrue(prev.compareTo(cur) <= 0);
                }
                prev = cur;
            }
        }
    }

    @Test
    public void testMeld1() {
        testSoftHeapMeld(SIZE, 0.10,  Comparator.naturalOrder());
        testSoftHeapMeld(SIZE, 0.10, comparator);
        testSoftHeapMeld(SIZE, 0.25,  Comparator.naturalOrder());
        testSoftHeapMeld(SIZE, 0.25, comparator);
        testSoftHeapMeld(SIZE, 0.5,  Comparator.naturalOrder());
        testSoftHeapMeld(SIZE, 0.5, comparator);
        testSoftHeapMeld(SIZE, 0.75,  Comparator.naturalOrder());
        testSoftHeapMeld(SIZE, 0.75, comparator);
        testSoftHeapMeld(SIZE, 0.95,  Comparator.naturalOrder());
        testSoftHeapMeld(SIZE, 0.95, comparator);
    }

    @Test
    public void testMeld2() {
        testSoftHeapMeldSmallLarge(SIZE, 0.10,  Comparator.naturalOrder());
        testSoftHeapMeldSmallLarge(SIZE, 0.10, comparator);
        testSoftHeapMeldSmallLarge(SIZE, 0.25,  Comparator.naturalOrder());
        testSoftHeapMeldSmallLarge(SIZE, 0.25, comparator);
        testSoftHeapMeldSmallLarge(SIZE, 0.5,  Comparator.naturalOrder());
        testSoftHeapMeldSmallLarge(SIZE, 0.5, comparator);
        testSoftHeapMeldSmallLarge(SIZE, 0.75,  Comparator.naturalOrder());
        testSoftHeapMeldSmallLarge(SIZE, 0.75, comparator);
        testSoftHeapMeldSmallLarge(SIZE, 0.95,  Comparator.naturalOrder());
        testSoftHeapMeldSmallLarge(SIZE, 0.95, comparator);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMeldWrong1() {
        BinaryTreeSoftHeap<Integer, String> h1 = new BinaryTreeSoftHeap<>(0.5, comparator);
        BinaryTreeSoftHeap<Integer, String> h2 = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());

        h1.meld(h2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMeldWrong2() {
        BinaryTreeSoftHeap<Integer, String> h1 = new BinaryTreeSoftHeap<>(0.4, Comparator.naturalOrder());
        BinaryTreeSoftHeap<Integer, String> h2 = new BinaryTreeSoftHeap<>(0.7, Comparator.naturalOrder());

        h1.meld(h2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMeldWrong3() {
        BinaryTreeSoftHeap<Integer, String> h1 = new BinaryTreeSoftHeap<>(0.4, Comparator.naturalOrder());
        BinaryTreeSoftHeap<Integer, String> h2 = new BinaryTreeSoftHeap<>(0.4, comparator);

        h1.meld(h2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMeldWrong4() {
        BinaryTreeSoftHeap<Integer, String> h1 = new BinaryTreeSoftHeap<>(0.4, comparator);
        BinaryTreeSoftHeap<Integer, String> h2 = new BinaryTreeSoftHeap<>(0.4, (o1, o2) -> comparator.compare(o1, o2));

        h1.meld(h2);
    }

    @Test(expected = IllegalStateException.class)
    public void testMultipleMelds() {
        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        a.insert(10, "");
        a.insert(11, "");
        a.insert(12, "");
        a.insert(13, "");

        BinaryTreeSoftHeap<Integer, String> b = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        b.insert(14, "");
        b.insert(15, "");
        b.insert(16, "");
        b.insert(17, "");

        BinaryTreeSoftHeap<Integer, String> c = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        c.insert(18, "");
        c.insert(19, "");
        c.insert(20, "");
        c.insert(21, "");

        a.meld(b);
        a.meld(b);
        validateSoftHeap(a, 0.5, 12L);
    }

    @Test
    public void testDeleteAfterMultipleMelds() {
        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        a.insert(10, "");
        a.insert(11, "");
        a.insert(12, "");
        a.insert(13, "");

        BinaryTreeSoftHeap<Integer, String> b = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        b.insert(14, "");
        b.insert(15, "");
        AddressableHeap.Handle<Integer, String> b3 = b.insert(16, "");
        b.insert(17, "");

        BinaryTreeSoftHeap<Integer, String> c = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        c.insert(18, "");
        c.insert(19, "");
        AddressableHeap.Handle<Integer, String> c3 = c.insert(20, "");
        c.insert(21, "");

        a.meld(b);
        a.meld(c);
        validateSoftHeap(a, 0.5, 12L);
        b3.delete();
        validateSoftHeap(a, 0.5, 12L);
        c3.delete();
        validateSoftHeap(a, 0.5, 12L);
    }

    @Test(expected = IllegalStateException.class)
    public void testInsertAfterAMeld() {
        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        a.insert(10, "");
        a.insert(11, "");
        a.insert(12, "");
        a.insert(13, "");

        BinaryTreeSoftHeap<Integer, String> b = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        b.insert(14, "");
        b.insert(15, "");
        b.insert(16, "");
        b.insert(17, "");

        a.meld(b);
        b.insert(30, "");
    }

    @Test
    public void testCascadingMelds() {
        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        a.insert(10, "");
        a.insert(11, "");
        a.insert(12, "");
        a.insert(13, "");

        BinaryTreeSoftHeap<Integer, String> b = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        b.insert(14, "");
        b.insert(15, "");
        b.insert(16, "");
        b.insert(17, "");

        BinaryTreeSoftHeap<Integer, String> c = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        c.insert(18, "");
        c.insert(19, "");
        c.insert(20, "");
        c.insert(21, "");

        BinaryTreeSoftHeap<Integer, String> d = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        d.insert(22, "");
        d.insert(23, "");
        d.insert(24, "");
        d.insert(25, "");

        BinaryTreeSoftHeap<Integer, String> e = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        e.insert(26, "");
        e.insert(27, "");
        e.insert(28, "");
        e.insert(29, "");

        d.meld(e);
        c.meld(d);
        b.meld(c);
        a.meld(b);

        assertEquals(20, a.size());
        assertEquals(0, b.size());
        assertEquals(0, c.size());
        assertEquals(0, d.size());
        assertEquals(0, e.size());

        validateSoftHeap(a, 0.5, 20L);
    }

    @Test
    public void testDeleteAfterCascadingMelds() {
        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        a.insert(10, "");
        a.insert(11, "");
        AddressableHeap.Handle<Integer, String> a3 = a.insert(12, "");
        a.insert(13, "");

        BinaryTreeSoftHeap<Integer, String> b = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        b.insert(14, "");
        b.insert(15, "");
        AddressableHeap.Handle<Integer, String> b3 = b.insert(16, "");
        b.insert(17, "");

        BinaryTreeSoftHeap<Integer, String> c = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        c.insert(18, "");
        c.insert(19, "");
        AddressableHeap.Handle<Integer, String> c3 = c.insert(20, "");
        c.insert(21, "");

        BinaryTreeSoftHeap<Integer, String> d = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        d.insert(22, "");
        d.insert(23, "");
        AddressableHeap.Handle<Integer, String> d3 = d.insert(24, "");
        d.insert(25, "");

        BinaryTreeSoftHeap<Integer, String> e = new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder());
        e.insert(26, "");
        e.insert(27, "");
        AddressableHeap.Handle<Integer, String> e3 = e.insert(28, "");
        e.insert(29, "");

        d.meld(e);
        c.meld(d);
        b.meld(c);
        a.meld(b);

        assertEquals(20, a.size());
        assertEquals(0, b.size());
        assertEquals(0, c.size());
        assertEquals(0, d.size());
        assertEquals(0, e.size());

        e3.delete();
        validateSoftHeap(a, 0.5, 20L);
        d3.delete();
        validateSoftHeap(a, 0.5, 20L);
        c3.delete();
        validateSoftHeap(a, 0.5, 20L);
        b3.delete();
        validateSoftHeap(a, 0.5, 20L);
        a3.delete();
        validateSoftHeap(a, 0.5, 20L);
    }

    @Test(expected = NullPointerException.class)
    public void testInsertNullKey() {
        new BinaryTreeSoftHeap<>(0.5, Comparator.naturalOrder()).insert(null, null);
    }

    private void testSoftHeapMeld(int n, double epsilon, Comparator<Integer> comparator) {
        BinaryTreeSoftHeap<Integer, String> h1 = new BinaryTreeSoftHeap<>(epsilon,
                comparator);
        BinaryTreeSoftHeap<Integer, String> h2 = new BinaryTreeSoftHeap<>(epsilon,
                comparator);

        for (int i = 0; i < n; i++) {
            if (i % 2 == 0) {
                h1.insert(i, "");
            } else {
                h2.insert(i, "");
            }
        }

        validateSoftHeap(h1, epsilon, n / 2);
        assertEquals(n / 2, h1.size());
        validateSoftHeap(h2, epsilon, n / 2);
        assertEquals(n / 2, h2.size());

        h1.meld(h2);

        validateSoftHeap(h1, epsilon, n);
        validateSoftHeap(h2, epsilon, n);
    }

    private void testSoftHeapMeldSmallLarge(int n, double epsilon, Comparator<Integer> comparator) {
        BinaryTreeSoftHeap<Integer, String> h1 = new BinaryTreeSoftHeap<>(epsilon,
                comparator);
        BinaryTreeSoftHeap<Integer, String> h2 = new BinaryTreeSoftHeap<>(epsilon,
                comparator);

        for (int i = 0; i < n / 3; i++) {
            h1.insert(i, "");
        }

        for (int i = n / 3; i < n; i++) {
            h2.insert(i, "");
        }

        validateSoftHeap(h1, epsilon, n / 3);
        assertEquals(n / 3, h1.size());
        validateSoftHeap(h2, epsilon, 2 * n / 3);
        assertEquals((int) Math.ceil(2.0 * n / 3.0), h2.size());

        h1.meld(h2);

        validateSoftHeap(h1, epsilon, n);
        validateSoftHeap(h2, epsilon, n);
    }

    private void testSoftHeapInsert(double epsilon, Comparator<Integer> comparator) {
        final int n = SIZE;

        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(epsilon,
                comparator);

        for (int i = 0; i < n; i++) {
            a.insert(i, "");
        }

        validateSoftHeap(a, epsilon, n);
    }

    private void testSoftHeapInsertDeleteMin(double epsilon, Comparator<Integer> comparator) {
        final int n = SIZE;

        BinaryTreeSoftHeap<Integer, String> a = new BinaryTreeSoftHeap<>(epsilon,
                comparator);

        for (int i = 0; i < n; i++) {
            a.insert(i, "");
        }

        for (int i = 0; i < n / 4; i++) {
            a.deleteMin();
        }

        validateSoftHeap(a, epsilon, n);
    }

    @SuppressWarnings("unchecked")
    private void testSoftHeapInsertDelete(double epsilon, Comparator<Integer> comparator) {
        final int n = SIZE;

        BinaryTreeSoftHeap<Integer, String> h = new BinaryTreeSoftHeap<>(epsilon,
                comparator);

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[n];
        for (int i = 0; i < n; i++) {
            array[i] = h.insert(i, "");
        }

        for (int i = 0; i < n / 4; i++) {
            array[i].delete();
        }

        validateSoftHeap(h, epsilon, n);
    }

    @SuppressWarnings("unchecked")
    private void testSoftHeapInsertDeleteDeleteMin(double epsilon, Comparator<Integer> comparator) {
        final int n = SIZE;

        BinaryTreeSoftHeap<Integer, String> h = new BinaryTreeSoftHeap<>(epsilon, comparator);

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[n];
        for (int i = 0; i < n; i++) {
            array[i] = h.insert(i, "");
        }

        for (int i = 0; i < n / 4; i++) {
            h.deleteMin();
        }

        for (int i = n - 1; i >= 3 * n / 4; i--) {
            try {
                array[i].delete();
            } catch (IllegalArgumentException e) {
                // ignore, already deleted due to corruption
            }
        }

        validateSoftHeap(h, epsilon, n);
    }

    /**
     * Validate the invariants of a soft heap.
     *
     * @param h
     *            the soft heap
     * @param epsilon
     *            the error rate of the soft heap
     * @param totalInsertedElements
     *            the total number of elements added in the heap
     */
    @SuppressWarnings("unchecked")
    private static <K, V> void validateSoftHeap(BinaryTreeSoftHeap<K, V> h, double epsilon,
                                                long totalInsertedElements) {
        long total = 0;
        long corrupted = 0;
        Comparator<? super K> comparator = h.comparator();
        BinaryTreeSoftHeap.RootListNode<K, V> cur = h.rootList.head;
        while (cur != null) {
            // validate each heap
            KeyCount kc = validateRoot(cur.root, comparator);
            // keep total count of total and corrupted elements
            total += kc.total;
            corrupted += kc.corrupted;
            cur = cur.next;
        }
        assertEquals(total, h.size());
        assertTrue("Too many corrupted elemenets", corrupted <= totalInsertedElements * epsilon);

        // validate suffix min pointers
        K minSoFar = null;
        cur = h.rootList.tail;
        while (cur != null) {
            // find min by hand
            if (minSoFar == null) {
                minSoFar = cur.root.cKey;
            } else {
                if (comparator == null) {
                    if (((Comparable<? super K>) cur.root.cKey).compareTo(minSoFar) <= 0) {
                        minSoFar = cur.root.cKey;
                    }
                } else {
                    if (comparator.compare(cur.root.cKey, minSoFar) <= 0) {
                        minSoFar = cur.root.cKey;
                    }
                }
            }

            // compare with suffix min
            assertEquals(minSoFar, cur.suffixMin.root.cKey);

            // keep total count of total and corrupted elements
            cur = cur.prev;
        }

    }

    @SuppressWarnings("unchecked")
    private static <K, V> KeyCount validateRoot(BinaryTreeSoftHeap.TreeNode<K, V> root, Comparator<? super K> comparator) {
        assertTrue(root.parent != null);
        if (root.left != null) {
            assertEquals(root.left.parent, root);
            assertTrue(root.rank > root.left.rank);
            if (comparator == null) {
                assertTrue(((Comparable<? super K>) root.cKey).compareTo(root.left.cKey) <= 0);
            } else {
                assertTrue(comparator.compare(root.cKey, root.left.cKey) <= 0);
            }
        }
        if (root.right != null) {
            assertEquals(root.right.parent, root);
            assertTrue(root.rank > root.right.rank);
            if (comparator == null) {
                assertTrue(((Comparable<? super K>) root.cKey).compareTo(root.right.cKey) <= 0);
            } else {
                assertTrue(comparator.compare(root.cKey, root.right.cKey) <= 0);
            }
        }
        if (root.left != null && root.right != null) {
            assertEquals(root.left.rank, root.right.rank);
        }
        assertTrue(root.cKey != null);
        assertTrue(root.cHead != null);
        assertTrue(root.cHead.tree == root);

        assertTrue(root.cSize > 0);
        long total = 0;
        long corrupted = 0;
        BinaryTreeSoftHeap.SoftHandle<K, V> e = root.cHead;
        while (e != null) {
            total++;
            if (comparator == null) {
                if (((Comparable<? super K>) e.key).compareTo(root.cKey) < 0) {
                    corrupted++;
                }
            } else {
                if (comparator.compare(e.key, root.cKey) < 0) {
                    corrupted++;
                }
            }
            e = e.next;
        }
        /*
         * Due to ghost elements (if delete operation is used)
         */
        assertTrue(total > 0 && total <= root.cSize);

        if (root.left != null) {
            KeyCount leftKeyCount = validateRoot(root.left, comparator);
            total += leftKeyCount.total;
            corrupted += leftKeyCount.corrupted;
        }

        if (root.right != null) {
            KeyCount rightKeyCount = validateRoot(root.right, comparator);
            total += rightKeyCount.total;
            corrupted += rightKeyCount.corrupted;
        }
        return new KeyCount(total, corrupted);
    }

    private static class KeyCount {
        long total;
        long corrupted;

        KeyCount(long total, long corrupted) {
            this.total = total;
            this.corrupted = corrupted;
        }
    }
}
