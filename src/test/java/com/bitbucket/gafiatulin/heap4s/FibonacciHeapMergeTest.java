package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class FibonacciHeapMergeTest extends MergeableHeapTest {

    protected MergeableHeap<Integer, String> createHeap() {
        return new FibonacciHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected MergeableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new FibonacciHeap<>(comparator);
    }

}
