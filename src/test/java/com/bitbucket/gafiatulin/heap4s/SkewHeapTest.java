package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class SkewHeapTest extends AddressableHeapTest {
    @Override
    protected AddressableHeap<Integer, String> createHeap() {
        return new SkewHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new SkewHeap<>(comparator);
    }

    @Override
    protected AddressableHeap<Integer, String> createHeapWithStringValues() {
        return new SkewHeap<>(Comparator.naturalOrder());
    }
}
