package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class LeftistHeapMergeTest extends MergeableHeapTest {

    @Override
    protected MergeableHeap<Integer, String> createHeap() {
        return new LeftistHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected MergeableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new LeftistHeap<>(comparator);
    }

}