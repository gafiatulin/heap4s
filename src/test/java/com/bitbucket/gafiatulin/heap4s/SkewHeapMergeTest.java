package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class SkewHeapMergeTest extends MergeableHeapTest {
    @Override
    protected MergeableHeap<Integer, String> createHeap() {
        return new SkewHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected MergeableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new SkewHeap<>(comparator);
    }
}
