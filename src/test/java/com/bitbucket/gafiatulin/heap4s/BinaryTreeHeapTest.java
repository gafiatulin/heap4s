package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class BinaryTreeHeapTest extends AddressableHeapTest {

    @Override
    protected AddressableHeap<Integer, String> createHeap() {
        return new BinaryTreeHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new BinaryTreeHeap<>(comparator);
    }

    @Override
    protected AddressableHeap<Integer, String> createHeapWithStringValues() {
        return new BinaryTreeHeap<>(Comparator.naturalOrder());
    }
}
