package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class CostlessMeldPairingHeapMergeTest extends MergeableHeapTest {
    protected MergeableHeap<Integer, String> createHeap() {
        return new CostlessMeldPairingHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected MergeableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new CostlessMeldPairingHeap<>(comparator);
    }
}
