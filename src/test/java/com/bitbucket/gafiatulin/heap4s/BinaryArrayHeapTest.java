package com.bitbucket.gafiatulin.heap4s;

import org.junit.Test;

import java.util.Comparator;

public class BinaryArrayHeapTest extends AddressableHeapTest {
    @Override
    protected AddressableHeap<Integer, String> createHeap() {
        return new BinaryArrayHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new BinaryArrayHeap<>(comparator);
    }

    @Override
    protected AddressableHeap<Integer, String> createHeapWithStringValues() {
        return new BinaryArrayHeap<>(Comparator.naturalOrder());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegal1() {
        new BinaryArrayHeap<Integer, String>(Comparator.naturalOrder(), -5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegal2() {
        new BinaryArrayHeap<Integer, String>(Comparator.naturalOrder(), Integer.MAX_VALUE);
    }
}
