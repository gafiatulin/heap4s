package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;

public class CostlessMeldPairingHeapTest extends AddressableHeapTest{
    @Override
    protected AddressableHeap<Integer, String> createHeap() {
        return new CostlessMeldPairingHeap<>(Comparator.naturalOrder());
    }

    @Override
    protected AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator) {
        return new CostlessMeldPairingHeap<>(comparator);
    }

    @Override
    protected AddressableHeap<Integer, String> createHeapWithStringValues() {
        return new CostlessMeldPairingHeap<>(Comparator.naturalOrder());
    }
}
