package com.bitbucket.gafiatulin.heap4s;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Consumer;


import org.junit.BeforeClass;
import org.junit.Test;
import scala.Tuple2;

import static org.junit.Assert.*;

public abstract class AddressableHeapTest {
    protected static final int SIZE = 100000;

    protected static Comparator<Integer> comparator;

    private static class TestComparator implements Comparator<Integer>, Serializable {
        private static final long serialVersionUID = 1L;

        @Override
        public int compare(Integer o1, Integer o2) {
            if (o1 < o2) {
                return 1;
            } else if (o1 > o2) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    @BeforeClass
    public static void setUpClass() {
        comparator = new TestComparator();
    }

    protected abstract AddressableHeap<Integer, String> createHeap();

    protected abstract AddressableHeap<Integer, String> createHeap(Comparator<Integer> comparator);

    protected abstract AddressableHeap<Integer, String> createHeapWithStringValues();

    @Test
    public void test() {
        AddressableHeap<Integer, String> h = createHeap();

        for (int i = 0; i < SIZE; i++) {
            h.insert(i, "");
            assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
            assertFalse(h.isEmpty());
            assertEquals(h.size(), i + 1);
        }

        for (int i = SIZE - 1; i >= 0; i--) {
            assertEquals(h.findMin().get().getKey(), Integer.valueOf(SIZE - i - 1));
            h.deleteMin();
        }
    }

    @Test
    public void testOnlyInsert() {
        AddressableHeap<Integer, String> h = createHeap();

        for (int i = 0; i < SIZE; i++) {
            h.insert(SIZE - i, "");
            assertEquals(Integer.valueOf(SIZE - i), h.findMin().get().getKey());
            assertFalse(h.isEmpty());
            assertEquals(h.size(), i + 1);
        }
    }

    @Test
    public void testSetValue() {
        AddressableHeap<Integer, String> h = createHeapWithStringValues();
        h.insert(1, "value1").setValue("value2");
        assertEquals("value2", h.findMin().get().getValue());
    }

    @Test
    public void testIterator(){
        Random generator = new Random(1);
        int[] ref = new int[SIZE*10];
        AddressableHeap<Integer, String> h = createHeap();
        for (int i = 0; i < SIZE*10; i++) {
            String v = String.valueOf(i);
            int key = generator.nextInt();
            h.insert(key, v);
            ref[i] = key;
        }
        int[] result = new int[SIZE*10];
        Iterator<AddressableHeap.Handle<Integer, String>> iterator = h.iterator();
        int i = 0;
        while(iterator.hasNext()){
            result[i] = iterator.next().getKey();
            i++;
        }
        assertEquals(i, h.size());
        Arrays.sort(result);
        Arrays.sort(ref);
        assertArrayEquals(ref, result);
        assertFalse(createHeap().iterator().hasNext());

    }

    @Test
    public void testOnly4() {

        AddressableHeap<Integer, String> h = createHeap();

        assertTrue(h.isEmpty());

        h.insert(780, "");
        assertEquals(h.size(), 1);
        assertEquals(Integer.valueOf(780), h.findMin().get().getKey());

        h.insert(-389, "");
        assertEquals(h.size(), 2);
        assertEquals(Integer.valueOf(-389), h.findMin().get().getKey());

        h.insert(306, "");
        assertEquals(h.size(), 3);
        assertEquals(Integer.valueOf(-389), h.findMin().get().getKey());

        h.insert(579, "");
        assertEquals(h.size(), 4);
        assertEquals(Integer.valueOf(-389), h.findMin().get().getKey());

        h.deleteMin();
        assertEquals(h.size(), 3);
        assertEquals(Integer.valueOf(306), h.findMin().get().getKey());

        h.deleteMin();
        assertEquals(h.size(), 2);
        assertEquals(Integer.valueOf(579), h.findMin().get().getKey());

        h.deleteMin();
        assertEquals(h.size(), 1);
        assertEquals(Integer.valueOf(780), h.findMin().get().getKey());

        h.deleteMin();
        assertEquals(h.size(), 0);

        assertTrue(h.isEmpty());
    }

    @Test
    public void testOnly4Reverse() {
        AddressableHeap<Integer, String> h = createHeap(comparator);

        assertTrue(h.isEmpty());

        h.insert(780, "");
        assertEquals(h.size(), 1);
        assertEquals(Integer.valueOf(780), h.findMin().get().getKey());

        h.insert(-389, "");
        assertEquals(h.size(), 2);
        assertEquals(Integer.valueOf(780), h.findMin().get().getKey());

        h.insert(306, "");
        assertEquals(h.size(), 3);
        assertEquals(Integer.valueOf(780), h.findMin().get().getKey());

        h.insert(579, "");
        assertEquals(h.size(), 4);
        assertEquals(Integer.valueOf(780), h.findMin().get().getKey());

        h.deleteMin();
        assertEquals(h.size(), 3);
        assertEquals(Integer.valueOf(579), h.findMin().get().getKey());

        h.deleteMin();
        assertEquals(h.size(), 2);
        assertEquals(Integer.valueOf(306), h.findMin().get().getKey());

        h.deleteMin();
        assertEquals(h.size(), 1);
        assertEquals(Integer.valueOf(-389), h.findMin().get().getKey());

        h.deleteMin();
        assertEquals(h.size(), 0);

        assertTrue(h.isEmpty());
    }

    @Test
    public void testSortRandomSeed1() {
        AddressableHeap<Integer, String> h = createHeap();

        Random generator = new Random(1);

        for (int i = 0; i < SIZE; i++) {
            h.insert(generator.nextInt(), "");
        }

        Integer prev = null, cur;
        while (!h.isEmpty()) {
            cur = h.findMin().get().getKey();
            h.deleteMin();
            if (prev != null) {
                assertTrue(prev.compareTo(cur) <= 0);
            }
            prev = cur;
        }
    }

    @Test
    public void testSort1RandomSeed1() {
        AddressableHeap<Integer, String> h = createHeap();

        Random generator = new Random(1);

        for (int i = 0; i < SIZE; i++) {
            h.insert(generator.nextInt(), "");
        }

        Integer prev = null, cur;
        while (!h.isEmpty()) {
            cur = h.deleteMin().get().getKey();
            if (prev != null) {
                assertTrue(prev.compareTo(cur) <= 0);
            }
            prev = cur;
        }
    }

    @Test
    public void testSortRandomSeed2() {
        AddressableHeap<Integer, String> h = createHeap();

        Random generator = new Random(2);

        for (int i = 0; i < SIZE; i++) {
            h.insert(generator.nextInt(), "");
        }

        Integer prev = null, cur;
        while (!h.isEmpty()) {
            cur = h.findMin().get().getKey();
            h.deleteMin();
            if (prev != null) {
                assertTrue(prev.compareTo(cur) <= 0);
            }
            prev = cur;
        }
    }

    @Test
    public void testSort2RandomSeed2() {
        AddressableHeap<Integer, String> h = createHeap();

        Random generator = new Random(2);

        for (int i = 0; i < SIZE; i++) {
            h.insert(generator.nextInt(), "");
        }

        Integer prev = null, cur;
        while (!h.isEmpty()) {
            cur = h.deleteMin().get().getKey();
            if (prev != null) {
                assertTrue(prev.compareTo(cur) <= 0);
            }
            prev = cur;
        }
    }

    @Test
    public void testFindMinDeleteMinSameObject() {
        AddressableHeap<Integer, String> h = createHeap();

        Random generator = new Random(1);

        for (int i = 0; i < SIZE; i++) {
            h.insert(generator.nextInt(), "");
        }

        while (!h.isEmpty()) {
            assertEquals(h.findMin(), h.deleteMin());
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDelete() {
        AddressableHeap<Integer, String> h = createHeap();

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[15];
        for (int i = 0; i < 15; i++) {
            array[i] = h.insert(i, "");
        }

        array[5].delete();
        assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
        array[7].delete();
        assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
        array[0].delete();
        assertEquals(Integer.valueOf(1), h.findMin().get().getKey());
        array[2].delete();
        assertEquals(Integer.valueOf(1), h.findMin().get().getKey());
        array[1].delete();
        assertEquals(Integer.valueOf(3), h.findMin().get().getKey());
        array[3].delete();
        assertEquals(Integer.valueOf(4), h.findMin().get().getKey());
        array[9].delete();
        assertEquals(Integer.valueOf(4), h.findMin().get().getKey());
        array[4].delete();
        assertEquals(Integer.valueOf(6), h.findMin().get().getKey());
        array[8].delete();
        assertEquals(Integer.valueOf(6), h.findMin().get().getKey());
        array[11].delete();
        assertEquals(Integer.valueOf(6), h.findMin().get().getKey());
        array[6].delete();
        assertEquals(Integer.valueOf(10), h.findMin().get().getKey());
        array[12].delete();
        assertEquals(Integer.valueOf(10), h.findMin().get().getKey());
        array[10].delete();
        assertEquals(Integer.valueOf(13), h.findMin().get().getKey());
        array[13].delete();
        assertEquals(Integer.valueOf(14), h.findMin().get().getKey());
        array[14].delete();
        assertTrue(h.isEmpty());

    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDelete1() {
        AddressableHeap<Integer, String> h = createHeap();

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[8];
        for (int i = 0; i < 8; i++) {
            array[i] = h.insert(i, "");
        }

        array[5].delete();
        assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
        array[7].delete();
        assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
        array[0].delete();
        assertEquals(Integer.valueOf(1), h.findMin().get().getKey());
        array[2].delete();
        assertEquals(Integer.valueOf(1), h.findMin().get().getKey());
        array[1].delete();
        assertEquals(Integer.valueOf(3), h.findMin().get().getKey());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testAddDelete() {
        AddressableHeap<Integer, String> h = createHeap();

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[SIZE];
        for (int i = 0; i < SIZE; i++) {
            array[i] = h.insert(i, "");
        }

        for (int i = SIZE - 1; i >= 0; i--) {
            array[i].delete();
            if (i > 0) {
                assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
            }
        }
        assertTrue(h.isEmpty());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testAddDeleteComparator() {
        AddressableHeap<Integer, String> h = createHeap(comparator);

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[SIZE];
        for (int i = 0; i < SIZE; i++) {
            array[i] = h.insert(i, "");
        }

        for (int i = 0; i < SIZE; i++) {
            array[i].delete();
            if (i < SIZE - 1) {
                assertEquals(Integer.valueOf(SIZE - 1), h.findMin().get().getKey());
            }
        }
        assertTrue(h.isEmpty());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testAddDecreaseKeyDeleteMin() {
        AddressableHeap<Integer, String> h = createHeap();

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[SIZE];
        for (int i = 0; i < SIZE; i++) {
            array[i] = h.insert(i, "");
        }

        for (int i = SIZE / 2; i < SIZE / 2 + 10; i++) {
            array[i].decreaseKey(i / 2);
        }

        array[0].delete();

        for (int i = SIZE / 2 + 10; i < SIZE / 2 + 20; i++) {
            array[i].decreaseKey(0);
        }

        assertEquals(0, h.deleteMin().get().getKey().intValue());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testAddDecreaseKeyDeleteMinComparator() {
        AddressableHeap<Integer, String> h = createHeap(comparator);

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[SIZE];
        for (int i = 0; i < SIZE; i++) {
            array[i] = h.insert(i, "");
        }

        for (int i = SIZE / 2; i < SIZE / 2 + 10; i++) {
            array[i].decreaseKey(SIZE - 1);
        }

        array[SIZE - 1].delete();

        for (int i = SIZE / 2 + 10; i < SIZE / 2 + 20; i++) {
            array[i].decreaseKey(SIZE - 1);
        }

        assertEquals(SIZE - 1, h.deleteMin().get().getKey().intValue());
    }

    @Test
    public void testClear() {
        AddressableHeap<Integer, String> h = createHeap();

        for (int i = 0; i < 15; i++) {
            h.insert(i, "");
        }

        h.clear();
        assertEquals(0L, h.size());
        assertTrue(h.isEmpty());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDeleteTwice() {

        AddressableHeap<Integer, String> h = createHeap();

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[15];
        for (int i = 0; i < 15; i++) {
            array[i] = h.insert(i, "");
        }

        array[5].delete();
        assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
        array[7].delete();
        assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
        array[0].delete();
        assertEquals(Integer.valueOf(1), h.findMin().get().getKey());
        array[2].delete();
        assertEquals(Integer.valueOf(1), h.findMin().get().getKey());
        array[1].delete();
        assertEquals(Integer.valueOf(3), h.findMin().get().getKey());
        array[3].delete();
        assertEquals(Integer.valueOf(4), h.findMin().get().getKey());
        array[9].delete();
        assertEquals(Integer.valueOf(4), h.findMin().get().getKey());
        array[4].delete();
        assertEquals(Integer.valueOf(6), h.findMin().get().getKey());

        // again
        assertFalse(array[2].delete());
    }

    @Test
    public void testDeleteMinDeleteTwice() {
        AddressableHeap<Integer, String> h = createHeap();
        AddressableHeap.Handle<Integer, String> e1 = h.insert(50, "");
        h.insert(100, "");
        assertEquals(h.deleteMin().get(), e1);
        assertFalse(e1.delete());
    }

    @Test
    public void testDeleteMinDeleteTwice1() {
        AddressableHeap<Integer, String> h = createHeap();

        for (int i = 100; i < 200; i++) {
            h.insert(i, "");
        }

        assertFalse(h.deleteMin().get().delete());
    }

    @Test
    public void testDeleteMinDecreaseKey() {
        AddressableHeap<Integer, String> h = createHeap();

        for (int i = 100; i < 200; i++) {
            h.insert(i, "");
        }
        assertFalse(h.deleteMin().get().decreaseKey(0));
    }

    @Test
    public void testNoElementFindMin() {
        AddressableHeap<Integer, String> h = createHeap();
        assertTrue(h.findMin().isEmpty());
    }

    @Test
    public void testNoElementDeleteMin() {
        AddressableHeap<Integer, String> h = createHeap();
        assertTrue(h.deleteMin().isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void testInsertNull() {
        AddressableHeap<Integer, String> h = createHeap();
        h.insert(null, null);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDecreaseKey() {

        AddressableHeap<Integer, String> h = createHeap();

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[15];
        for (int i = 0; i < 15; i++) {
            array[i] = h.insert(i + 100, "");
        }

        assertEquals(Integer.valueOf(100), h.findMin().get().getKey());
        array[5].decreaseKey(5);
        assertEquals(Integer.valueOf(5), h.findMin().get().getKey());
        array[1].decreaseKey(50);
        assertEquals(Integer.valueOf(5), h.findMin().get().getKey());
        array[1].decreaseKey(20);
        assertEquals(Integer.valueOf(5), h.findMin().get().getKey());
        array[5].delete();
        assertEquals(Integer.valueOf(20), h.findMin().get().getKey());
        array[10].decreaseKey(3);
        assertEquals(Integer.valueOf(3), h.findMin().get().getKey());
        array[0].decreaseKey(0);
        assertEquals(Integer.valueOf(0), h.findMin().get().getKey());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDecreaseKeyWithComparator1() {
        AddressableHeap<Integer, String> h = createHeap(comparator);

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[15];
        for (int i = 0; i < 15; i++) {
            array[i] = h.insert(i, "");
        }

        assertEquals(Integer.valueOf(14), h.findMin().get().getKey());
        array[5].decreaseKey(205);
        assertEquals(Integer.valueOf(205), h.findMin().get().getKey());
        array[1].decreaseKey(250);
        assertEquals(Integer.valueOf(250), h.findMin().get().getKey());
        array[1].decreaseKey(300);
        assertEquals(Integer.valueOf(300), h.findMin().get().getKey());
        array[5].delete();
        assertEquals(Integer.valueOf(300), h.findMin().get().getKey());
        array[10].decreaseKey(403);
        assertEquals(Integer.valueOf(403), h.findMin().get().getKey());
        array[0].decreaseKey(1000);
        assertEquals(Integer.valueOf(1000), h.findMin().get().getKey());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDecreaseKey1() {
        AddressableHeap<Integer, String> h = createHeap();

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[1000];
        for (int i = 0; i < 1000; i++) {
            array[i] = h.insert(2000 + i, "");
        }

        for (int i = 999; i >= 0; i--) {
            array[i].decreaseKey(array[i].getKey() - 2000);
        }

        for (int i = 0; i < 1000; i++) {
            assertEquals(i, h.deleteMin().get().getKey().intValue());
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDecreaseKeyWithComparator() {
        AddressableHeap<Integer, String> h = createHeap(comparator);

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[1000];
        for (int i = 0; i < 1000; i++) {
            array[i] = h.insert(i, "");
        }

        for (int i = 0; i < 1000; i++) {
            array[i].decreaseKey(array[i].getKey() + 2000);
        }

        for (int i = 999; i >= 0; i--) {
            assertEquals(i + 2000, h.deleteMin().get().getKey().intValue());
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testIncreaseKey() {

        AddressableHeap<Integer, String> h = createHeap();

        AddressableHeap.Handle<Integer, String> array[];
        array = new AddressableHeap.Handle[15];
        for (int i = 0; i < 15; i++) {
            array[i] = h.insert(i + 100, "");
        }

        assertEquals(Integer.valueOf(100), h.findMin().get().getKey());
        array[5].decreaseKey(5);
        assertEquals(Integer.valueOf(5), h.findMin().get().getKey());
        array[1].decreaseKey(102);
    }

    @Test
    public void testIncreaseKeyWithComparator() {
        AddressableHeap<Integer, String> h = createHeap(comparator);
        assertFalse(h.insert(10, "").decreaseKey(9));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSerializable() throws IOException, ClassNotFoundException {

        AddressableHeap<Integer, String> h = createHeap();

        for (int i = 0; i < 15; i++) {
            h.insert(i, "");
        }

        // write
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(h);
        oos.close();
        byte[] data = baos.toByteArray();

        // read

        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o = ois.readObject();
        ois.close();
        h = (AddressableHeap<Integer, String>) o;

        for (int i = 0; i < 15; i++) {
            assertEquals(15 - i, h.size());
            assertEquals(Integer.valueOf(i), h.findMin().get().getKey());
            h.deleteMin();
        }
        assertTrue(h.isEmpty());
    }

    @Test
    public void testSameKey() {
        AddressableHeap<Integer, String> h = createHeap();

        assertTrue(h.isEmpty());

        AddressableHeap.Handle<Integer, String> handle = h.insert(780, "");
        handle.decreaseKey(780);
        assertEquals(780, h.deleteMin().get().getKey().intValue());
        assertTrue(h.isEmpty());
    }

    @Test
    public void testGetValue() {
        AddressableHeap<Integer, String> h = createHeapWithStringValues();

        assertTrue(h.isEmpty());

        AddressableHeap.Handle<Integer, String> handle = h.insert(1, "1");
        assertEquals("1", handle.getValue());
    }

    @Test
    public void testComparator() {
        AddressableHeap<Integer, String> h = createHeap(comparator);
        int i;

        for (i = 0; i < SIZE; i++) {
            h.insert(i, "");
            assertEquals(Integer.valueOf(i), h.findMin().get().getKey());
            assertFalse(h.isEmpty());
            assertEquals(h.size(), i + 1);
        }

        for (i = SIZE - 1; i >= 0; i--) {
            assertEquals(h.findMin().get().getKey(), Integer.valueOf(i));
            h.deleteMin();
        }
    }

    @Test
    public void testDecreaseSame() {
        AddressableHeap<Integer, String> h = createHeap(comparator);
        h.insert(10, "").decreaseKey(10);
        assertEquals(10, h.findMin().get().getKey().intValue());
    }

    @Test
    public void testInvalidHandleDecreaseKey() {
        AddressableHeap<Integer, String> h = createHeap(comparator);
        AddressableHeap.Handle<Integer, String> handle = h.insert(10, "");
        assertTrue(h.deleteMin().isDefined());
        assertFalse(handle.decreaseKey(11));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testSerializableWithComparator() throws IOException, ClassNotFoundException {
        AddressableHeap<Integer, String> h = createHeap(comparator);

        for (int i = 0; i < 15; i++) {
            h.insert(i, "");
        }

        // write
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(h);
        oos.close();
        byte[] data = baos.toByteArray();

        // read

        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o = ois.readObject();
        ois.close();
        h = (AddressableHeap<Integer, String>) o;

        for (int i = 0; i < 15; i++) {
            assertEquals(15 - i, h.size());
            assertEquals(Integer.valueOf(15 - i - 1), h.findMin().get().getKey());
            h.deleteMin();
        }
        assertTrue(h.isEmpty());
    }
}
