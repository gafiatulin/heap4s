package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Option;

import java.io.Serializable;
import java.util.*;

/**
 * An explicit binary tree addressable heap. The heap is sorted by a
 * {@link Comparator} provided at heap creation time.
 *
 * <p>
 * The worst-case cost of {@code insert}, {@code deleteMin}, {@code delete} and
 * {@code decreaceKey} operations is O(log(n)) and the cost of {@code findMin}
 * is O(1).
 *
 * {@inheritDoc}
 *
 * @see AddressableHeap
 */

@NotThreadSafe
@TreeBased
public class BinaryTreeHeap<K, V> implements AddressableHeap<K, V>, Serializable {
    private final static long serialVersionUID = 1;
    private final Comparator<? super K> comparator;
    private long size;
    private Node root;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     *
     * <p>
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     */
    @ConstantTime
    public BinaryTreeHeap(Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        this.comparator = comparator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime
    @SuppressWarnings("unchecked")
    public Handle<K, V> insert(K key, V value) {
        if (key == null) {
            throw new NullPointerException("Null keys not permitted");
        }
        if (value == null){
            throw new NullPointerException("Null values not permitted");
        }
        Node n = new Node(key, value);

        // easy special cases
        if (size == 0) {
            root = n;
            size = 1;
            return n;
        } else if (size == 1) {
            int c = comparator.compare(key, root.key);
            if (c < 0) {
                n.oldestChild = root;
                root.parentOrYoungerSibling = n;
                root = n;
            } else {
                root.oldestChild = n;
                n.parentOrYoungerSibling = root;
            }
            size = 2;
            return n;
        }

        // find parent of last node and hang
        Node p = findParentNode(size + 1);
        if (p.oldestChild == null) {
            p.oldestChild = n;
        } else {
            p.oldestChild.parentOrYoungerSibling = n;
        }
        n.parentOrYoungerSibling = p;

        // increase size
        size++;

        // fix priorities
        fixUp(n);

        return n;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public Option<Handle<K, V>> findMin(){
        if (size == 0) {
            return Option.empty();
        }
        return Option.apply(root);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime
    public Option<Handle<K, V>> deleteMin() {
        if (size == 0) {
            return Option.empty();
        }
        Node oldRoot = root;

        // easy special cases
        if (size == 1) {
            root = null;
            size = 0;
            return Option.apply(oldRoot);
        } else if (size == 2) {
            root = root.oldestChild;
            root.oldestChild = null;
            root.parentOrYoungerSibling = null;
            size = 1;
            oldRoot.oldestChild = null;
            return Option.apply(oldRoot);
        }

        // remove last node
        Node lastNodeParent = findParentNode(size);
        Node lastNode = lastNodeParent.oldestChild;
        if (lastNode.parentOrYoungerSibling != lastNodeParent) {
            Node tmp = lastNode;
            lastNode = tmp.parentOrYoungerSibling;
            tmp.parentOrYoungerSibling = lastNodeParent;
        } else {
            lastNodeParent.oldestChild = null;
        }
        lastNode.parentOrYoungerSibling = null;

        // decrease size
        size--;

        // place it as root
        // (assumes root.oldestChild exists)
        if (root.oldestChild.parentOrYoungerSibling == root) {
            root.oldestChild.parentOrYoungerSibling = lastNode;
        } else {
            root.oldestChild.parentOrYoungerSibling.parentOrYoungerSibling = lastNode;
        }
        lastNode.oldestChild = root.oldestChild;
        root = lastNode;

        // fix priorities
        fixDown(root);

        oldRoot.oldestChild = null;
        return Option.apply(oldRoot);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LinearTime
    public Iterator<Handle<K, V>> iterator() {
        return new Iterator<Handle<K, V>>() {
            private Node current = root;
            private Deque<Node> stack = new ArrayDeque<>();

            public boolean hasNext() {
                return current != null;
            }

            public Handle<K, V> next() {
                Node previous = current;
                if(current.oldestChild!= null){
                    stack.push(current);
                    current = current.oldestChild;
                } else if(stack.peek() == current.parentOrYoungerSibling) {
                    stack.pop();
                    current = current.parentOrYoungerSibling.parentOrYoungerSibling;
                    while(stack.peek() == current && !stack.isEmpty()){
                        stack.pop();
                        current = current.parentOrYoungerSibling;
                    }
                } else {
                    current = current.parentOrYoungerSibling;
                }
                return previous;
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public long size() {
        return size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public void clear() {
        root = null;
        size = 0;
    }

    /**
     *  Handle
     */
    private class Node implements AddressableHeap.Handle<K, V>, Serializable {
        private final static long serialVersionUID = 1;
        K key;
        V value;
        Node oldestChild; // older child
        Node parentOrYoungerSibling; // younger sibling or parent

        Node(K key, V value) {
            this.key = key;
            this.value = value;
            this.oldestChild = null;
            this.parentOrYoungerSibling = null;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public void setValue(V value) {
            this.value = value;
        }

        @Override
        @LogarithmicTime
        @SuppressWarnings("unchecked")
        public boolean decreaseKey(K newKey) {
            if (this != root && parentOrYoungerSibling == null) {
                return false;
            }
            int c = comparator.compare(newKey, key);

            if (c > 0) {
                return false;
            }
            key = newKey;
            if (c == 0 || root == this) {
                return true;
            }
            fixUp(this);
            return true;
        }

        @Override
        @LogarithmicTime
        public boolean delete() {
            if (this != root && parentOrYoungerSibling == null) {
                return false;
            }

            Node p = getParent(this);
            while (p != null) {
                Node pp = getParent(p);
                swap(this, p, pp);
                p = pp;
            }

            // remove root
            deleteMin();
            oldestChild = null;
            parentOrYoungerSibling = null;
            return true;
        }
    }

    /* -------------------------------------------------------------------- */

    @SuppressWarnings("unchecked")
    private void fixUp(Node n) {
        Node p = getParent(n);
        while (p != null) {
            if (comparator.compare(n.key, p.key) >= 0) {
                break;
            }
            Node pp = getParent(p);
            swap(n, p, pp);
            p = pp;
        }
    }

    @SuppressWarnings("unchecked")
    private void fixDown(Node n) {
        Node p = getParent(n);
        while (n.oldestChild != null) {
            Node child = n.oldestChild;
            if (child.parentOrYoungerSibling != n && comparator.compare(child.parentOrYoungerSibling.key, child.key) < 0) {
                child = child.parentOrYoungerSibling;
            }
            if (comparator.compare(n.key, child.key) <= 0) {
                break;
            }
            swap(child, n, p);
            p = child;
        }
    }

    /**
     * Get the parent node of a given node.
     */
    private Node getParent(Node n) {
        if (n.parentOrYoungerSibling == null) {
            return null;
        }
        Node c = n.parentOrYoungerSibling;
        if (c.oldestChild == n) {
            return c;
        }
        Node p1 = c.parentOrYoungerSibling;
        if (p1 != null && p1.oldestChild == n) {
            return p1;
        }
        return c;
    }

    /**
     * Start at the root and traverse the tree in order to find the parent node
     * of a particular node. Uses the bit representation to keep the cost
     * log(n).
     *
     * @param node the node number assuming that the root node is number one
     */
    private Node findParentNode(long node) {
        // assert node > 0;

        // find bit representation of node
        long[] s = { node };
        BitSet bits = BitSet.valueOf(s);

        // traverse path to last node
        Node cur = root;
        for (int i = bits.length() - 2; i > 0; i--) {
            if (bits.get(i)) {
                cur = cur.oldestChild.parentOrYoungerSibling;
            } else {
                cur = cur.oldestChild;
            }
        }
        return cur;
    }

    /**
     * Swap a node with its parent which must be the root.
     */
    private void swap(Node n, Node root) {
        // assert this.root == root;

        Node nLeftChild = n.oldestChild;
        if (root.oldestChild == n) {
            if (n.parentOrYoungerSibling == root) {
                // n is left child and no right sibling
                n.oldestChild = root;
                root.parentOrYoungerSibling = n;
            } else {
                // n is left child and has right sibling
                root.parentOrYoungerSibling = n.parentOrYoungerSibling;
                root.parentOrYoungerSibling.parentOrYoungerSibling = n;
                n.oldestChild = root;
            }
        } else {
            // n is right child
            root.oldestChild.parentOrYoungerSibling = root;
            n.oldestChild = root.oldestChild;
            root.parentOrYoungerSibling = n;
        }
        n.parentOrYoungerSibling = null;

        // hang children
        root.oldestChild = nLeftChild;
        if (nLeftChild != null) {
            if (nLeftChild.parentOrYoungerSibling == n) {
                nLeftChild.parentOrYoungerSibling = root;
            } else {
                nLeftChild.parentOrYoungerSibling.parentOrYoungerSibling = root;
            }
        }
        this.root = n;
    }

    /**
     * Swap a node with its parent
     *
     * @param n the node
     *
     * @param p the parent node
     *
     * @param pp the parent of the parent node, maybe null
     */
    private void swap(Node n, Node p, Node pp) {
        if (pp == null) {
            swap(n, p);
            return;
        }

        Node nLeftChild = n.oldestChild;
        if (pp.oldestChild == p) {
            // p left child of pp
            if (p.oldestChild == n) {
                if (n.parentOrYoungerSibling == p) {
                    // n left child of p and no sibling
                    pp.oldestChild = n;
                    n.parentOrYoungerSibling = p.parentOrYoungerSibling;
                    n.oldestChild = p;
                    p.parentOrYoungerSibling = n;
                } else {
                    // n left child or p and sibling
                    n.parentOrYoungerSibling.parentOrYoungerSibling = n;
                    Node tmp = n.parentOrYoungerSibling;
                    n.parentOrYoungerSibling = p.parentOrYoungerSibling;
                    p.parentOrYoungerSibling = tmp;
                    pp.oldestChild = n;
                    n.oldestChild = p;
                }
            } else {
                // n right child of p
                Node tmp = p.oldestChild;
                n.parentOrYoungerSibling = p.parentOrYoungerSibling;
                pp.oldestChild = n;
                n.oldestChild = tmp;
                tmp.parentOrYoungerSibling = p;
                p.parentOrYoungerSibling = n;
            }
        } else {
            // p right child of pp
            if (p.oldestChild == n) {
                if (n.parentOrYoungerSibling == p) {
                    // n left child of p and no sibling
                    n.parentOrYoungerSibling = pp;
                    pp.oldestChild.parentOrYoungerSibling = n;
                    n.oldestChild = p;
                    p.parentOrYoungerSibling = n;
                } else {
                    // n left child of p and sibling
                    pp.oldestChild.parentOrYoungerSibling = n;
                    p.parentOrYoungerSibling = n.parentOrYoungerSibling;
                    n.parentOrYoungerSibling = pp;
                    n.oldestChild = p;
                    p.parentOrYoungerSibling.parentOrYoungerSibling = n;
                }
            } else {
                // n right child of p
                pp.oldestChild.parentOrYoungerSibling = n;
                n.parentOrYoungerSibling = pp;
                n.oldestChild = p.oldestChild;
                n.oldestChild.parentOrYoungerSibling = p;
                p.parentOrYoungerSibling = n;
            }
        }

        // hang children
        p.oldestChild = nLeftChild;
        if (nLeftChild != null) {
            if (nLeftChild.parentOrYoungerSibling == n) {
                nLeftChild.parentOrYoungerSibling = p;
            } else {
                nLeftChild.parentOrYoungerSibling.parentOrYoungerSibling = p;
            }
        }
    }

}
