package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Tuple2;

import java.io.Serializable;
import java.util.*;
/**
 * An array based d-ary addressable heap. The heap is sorted by a
 * {@link Comparator} provided at heap creation time.
 *
 * Constructing such a heap from an array of elements can be performed using the
 * method {@link #heapify(int, Object[], Object[], Comparator)} or
 * {@link #heapify(int, Tuple2[], Comparator)} in linear time.
 * <p>
 *
 * {@inheritDoc}
 *
 * @see AbstractArrayHeap
 */

@NotThreadSafe
@ArrayBased
public class DaryArrayHeap <K, V> extends AbstractArrayHeap<K, V> implements Serializable {
    private final static long serialVersionUID = 1;
    private int d;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     *
     * <p>
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * <p>
     * The initial capacity of the heap is {@link #DEFAULT_HEAP_CAPACITY} and
     * adjusts automatically based on the sequence of insertions and deletions.
     *
     * @param d the number of children of each node in the d-ary heap
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     * @throws IllegalArgumentException when number of children per node {@code d < 2}
     */
    public DaryArrayHeap(int d, Comparator<? super K> comparator) {
        this(d, comparator, DEFAULT_HEAP_CAPACITY);
    }

    /**
     * Constructs a new, empty heap, with a provided initial capacity ordered
     * according to the given comparator.
     *
     * <p>
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * <p>
     * The initial capacity of the heap is provided by the user and is adjusted
     * automatically based on the sequence of insertions and deletions. The
     * capacity will never become smaller than the initial requested capacity.
     *
     * @param d the number of children of each node in the d-ary heap
     * @param comparator the comparator that will be used to order this heap.
     * @param capacity the initial heap capacity
     *
     * @throws NullPointerException when {@code comparator} is null
     * @throws IllegalArgumentException when number of children per node {@code d < 2}
     * @throws IllegalArgumentException when @{code capacity < 0 || capacity > (Integer.MAX_VALUE - 8 - 1)}
     */
    public DaryArrayHeap(int d, Comparator<? super K> comparator, int capacity) {
        super(comparator, capacity);
        if (d < 2) {
            throw new IllegalArgumentException("D-ary heaps must have at least 2 children per node");
        }
        this.d = d;
    }

    /**
     * Create a heap from an array of elements. The elements of the array are
     * not destroyed. The method has linear time complexity.
     *
     * @param <K> the type of keys maintained by the heap
     * @param <V> the type of values maintained by the heap
     * @param d the number of children of the d-ary heap
     * @param keys an array of keys
     * @param values an array of values
     * @param comparator the comparator to use
     *
     * @throws NullPointerException when {@code comparator} or {@code keys} or {@code values} is null
     * @throws IllegalArgumentException when number of children per node {@code d < 2}
     * @throws IllegalArgumentException when {@code keys} and {@code values} have different sizes
     */
    @LinearTime
    public static <K, V> DaryArrayHeap<K, V> heapify(int d, K[] keys, V[] values, Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        if (keys == null) {
            throw new NullPointerException("Keys array cannot be null");
        }
        if(values == null){
            throw new NullPointerException("Values array cannot be null");
        }
        if (d < 2) {
            throw new IllegalArgumentException("D-ary heaps must have at least 2 children per node");
        }
        if (keys.length != values.length) {
            throw new IllegalArgumentException("Values array must have the same length as the keys array");
        }
        if (keys.length == 0) {
            return new DaryArrayHeap<>(d, comparator);
        }
        DaryArrayHeap<K, V> h = new DaryArrayHeap<>(d, comparator, keys.length);

        for (int i = 0; i < keys.length; i++) {
            K key = keys[i];
            V value = values[i];
            AbstractArrayHeap<K, V>.ArrayHandle ah = h.new ArrayHandle(key, value);
            ah.index = i + 1;
            h.underlyingArray[i + 1] = ah;
        }
        h.size = keys.length;

        for (int i = keys.length / d; i > 0; i--) {
            h.fixdownWithComparator(i);
        }
        return h;
    }

    /**
     * Create a heap from an array of elements. The elements of the array are
     * not destroyed. The method has linear time complexity.
     *
     * @param <K> the type of keys maintained by the heap
     * @param <V> the type of values maintained by the heap
     * @param d the number of children of the d-ary heap
     * @param kvs an array of key-value pairs
     * @param comparator the comparator to use
     *
     * @throws NullPointerException when {@code comparator} or {@code cvs} is null
     * @throws IllegalArgumentException when number of children per node {@code d < 2}
     */
    @LinearTime
    public static <K, V> DaryArrayHeap<K, V> heapify(int d, Tuple2<K, V>[] kvs, Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        if (kvs == null) {
            throw new NullPointerException("Key-value array cannot be null");
        }
        if (d < 2) {
            throw new IllegalArgumentException("D-ary heaps must have at least 2 children per node");
        }
        if (kvs.length == 0) {
            return new DaryArrayHeap<>(d, comparator);
        }
        DaryArrayHeap<K, V> h = new DaryArrayHeap<>(d, comparator, kvs.length);

        for (int i = 0; i < kvs.length; i++){
            Tuple2<K, V> kv = kvs[i];
            AbstractArrayHeap<K, V>.ArrayHandle ah = h.new ArrayHandle(kv._1, kv._2);
            ah.index = i + 1;
            h.underlyingArray[i + 1] = ah;
        }
        h.size = kvs.length;
        for (int i = kvs.length / d; i > 0; i--) {
            h.fixdownWithComparator(i);
        }
        return h;
    }

    @Override
    protected void forceFixup(int k) {
        ArrayHandle h = underlyingArray[k];
        while (k > 1) {
            int p = (k - 2) / d + 1;
            underlyingArray[k] = underlyingArray[p];
            underlyingArray[k].index = k;
            k = p;
        }
        underlyingArray[k] = h;
        h.index = k;
    }

    @Override
    protected void fixupWithComparator(int k) {
        ArrayHandle h = underlyingArray[k];
        while (k > 1) {
            int p = (k - 2) / d + 1;
            if (comparator.compare(underlyingArray[p].getKey(), h.getKey()) <= 0) {
                break;
            }
            underlyingArray[k] = underlyingArray[p];
            underlyingArray[k].index = k;
            k = p;
        }
        underlyingArray[k] = h;
        h.index = k;
    }

    @Override
    protected void fixdownWithComparator(int k) {
        int c;
        ArrayHandle h = underlyingArray[k];
        while ((c = d * (k - 1) + 2) <= size) {
            int maxc = c;
            for (int i = 1; i < d && c + i <= size; i++) {
                if (comparator.compare(underlyingArray[maxc].getKey(), underlyingArray[c + i].getKey()) > 0) {
                    maxc = c + i;
                }
            }
            if (comparator.compare(h.getKey(), underlyingArray[maxc].getKey()) <= 0) {
                break;
            }
            underlyingArray[k] = underlyingArray[maxc];
            underlyingArray[k].index = k;
            k = maxc;
        }
        underlyingArray[k] = h;
        h.index = k;
    }

}
