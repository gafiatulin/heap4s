package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Option;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Simple Fibonacci heaps. The heap is sorted by a
 * {@link Comparator} provided at heap creation time.
 *
 * <p>
 * This variant of the Fibonacci heaps is described in detail in the following
 * <a href="https://arxiv.org/abs/1407.5750">paper</a>:
 * <ul>
 * <li>Haim Kaplan, Robert E. Tarjan, Uri Zwick, Fibonacci Heaps Revisited,
 * arXiv 1407.5750, 2014.</li>
 * </ul>
 *
 * {@inheritDoc}
 *
 * @see FibonacciHeap
 * @see MergeableHeap
 */

@NotThreadSafe
@TreeBased
public class SimpleFibonacciHeap<K, V> implements MergeableHeap<K, V>, Serializable {
    private final static long serialVersionUID = 1;
    /**
     * Size of consolidation auxiliary array. Computed for number of elements
     * equal to {@link Long#MAX_VALUE}.
     */
    private final static int AUX_CONSOLIDATE_ARRAY_SIZE = 91;

    private final Comparator<? super K> comparator;
    private long size;
    private Node<K, V> root;

    /**
     * Auxiliary array for consolidation
     */
    private Node<K, V>[] aux;

    /**
     * Used to reference the current heap or some other heap in case of melding,
     * so that handles remain valid even after a meld, without having to iterate
     * over them.
     *
     * In order to avoid maintaining a full-fledged union-find data structure,
     * we disallow a heap to be used in melding more than once. We use however,
     * path-compression in case of cascading melds, that it, a handle moves from
     * one heap to another and then another.
     */
    protected SimpleFibonacciHeap<K, V> other;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     */
    @ConstantTime
    @SuppressWarnings("unchecked")
    public SimpleFibonacciHeap(Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        this.root = null;
        this.comparator = comparator;
        this.size = 0;
        this.aux = (Node<K, V>[]) Array.newInstance(Node.class, AUX_CONSOLIDATE_ARRAY_SIZE);
        this.other = this;
    }

    /**
     * {@inheritDoc}
     * @throws IllegalStateException when the heap has already been used in the right hand side of a meld
     */
    @Override
    @SuppressWarnings("unchecked")
    @ConstantTime(amortized = true)
    public AddressableHeap.Handle<K, V> insert(K key, V value) {
        if (key == null) {
            throw new NullPointerException("Null keys not permitted");
        }
        if (value == null){
            throw new NullPointerException("Null values not permitted");
        }
        if (other != this) {
            throw new IllegalStateException("A heap cannot be used after a meld");
        }
        Node<K, V> n = new Node<>(this, key, value);
        if (root == null) {
            root = n;
        } else {
            if (comparator.compare(n.key, root.key) < 0) {
                root = link(root, n);
            } else {
                link(n, root);
            }
        }
        size++;
        return n;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime(amortized = true)
    public Option<Handle<K, V>> findMin() {
        if (size == 0) {
            return Option.empty();
        }
        return Option.apply(root);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime(amortized = true)
    public Option<AddressableHeap.Handle<K, V>> deleteMin() {
        return comparatorDeleteMin();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LinearTime
    public Iterator<Handle<K, V>> iterator() {
        // TODO: 22/01/2018
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public long size() {
        return size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public void clear() {
        root = null;
        size = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    @ConstantTime(amortized = true)
    public void meld(MergeableHeap<K, V> other) {
        SimpleFibonacciHeap<K, V> h = (SimpleFibonacciHeap<K, V>) other;
        // check same comparator
        if (!h.comparator.equals(comparator)) {
            throw new IllegalArgumentException("Cannot meld heaps using different comparators!");
        }
        if (h.other != h) {
            throw new IllegalStateException("A heap cannot be used after a meld.");
        }

        // meld
        if (root == null) {
            root = h.root;
        } else if (h.root != null) {
            if (comparator.compare(h.root.key, root.key) < 0) {
                root = link(root, h.root);
            } else {
                link(h.root, root);
            }
        }
        size += h.size;

        // clear other
        h.size = 0;
        h.root = null;

        // take ownership
        h.other = this;
    }

    /**
     *  Handle
     */
    static class Node<K, V> implements AddressableHeap.Handle<K, V>, Serializable {
        private final static long serialVersionUID = 1;

        SimpleFibonacciHeap<K, V> heap;

        K key;
        V value;
        Node<K, V> parent; // parent
        Node<K, V> child; // any child
        Node<K, V> next; // younger sibling
        Node<K, V> prev; // older sibling
        int rank; // node rank
        boolean mark; // marked or not

        Node(SimpleFibonacciHeap<K, V> heap, K key, V value) {
            this.heap = heap;
            this.key = key;
            this.value = value;
            this.parent = null;
            this.child = null;
            this.next = this;
            this.prev = this;
            this.rank = 0;
            this.mark = false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K getKey() {
            return key;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public V getValue() {
            return value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setValue(V value) {
            this.value = value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @LogarithmicTime(amortized = true)
        public boolean delete() {
            if (this.next == null) {
                return false;
            }
            SimpleFibonacciHeap<K, V> h = getOwner();
            h.forceDecreaseKeyToMinimum(this);
            h.deleteMin();
            return true;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @ConstantTime(amortized = true)
        public boolean decreaseKey(K newKey) {
            SimpleFibonacciHeap<K, V> h = getOwner();
            return h.comparatorDecreaseKey(this, newKey);
        }

        /**
         * Get the owner heap of the handle. This is union-find with
         * path-compression between heaps.
         */
        private SimpleFibonacciHeap<K, V> getOwner() {
            if (heap.other != heap) {
                // find root
                SimpleFibonacciHeap<K, V> root = heap;
                while (root != root.other) {
                    root = root.other;
                }
                // path-compression
                SimpleFibonacciHeap<K, V> cur = heap;
                while (cur.other != root) {
                    SimpleFibonacciHeap<K, V> next = cur.other;
                    cur.other = root;
                    cur = next;
                }
                heap = root;
            }
            return heap;
        }
    }

    /* -------------------------------------------------------------------- */

    /**
     * Decrease the key of a node.
     */
    private boolean comparatorDecreaseKey(Node<K, V> n, K newKey) {
        int c = comparator.compare(newKey, n.key);
        if (c > 0) {
            return false;
        }
        n.key = newKey;
        if (c == 0) {
            return true;
        }

        if (n.next == null) {
            return false;
        }

        // if not root and heap order violation
        Node<K, V> y = n.parent;
        if (y != null && comparator.compare(n.key, y.key) < 0) {
            cut(n, y);
            root.mark = false;
            cascadingRankChange(y);
            if (comparator.compare(n.key, root.key) < 0) {
                root = link(root, n);
            } else {
                link(n, root);
            }
        }
        return true;
    }

    /**
     * Decrease the key of a node to the minimum. Helper function for performing
     * a delete operation. Does not change the node's actual key, but behaves as
     * the key is the minimum key in the heap.
     */
    private void forceDecreaseKeyToMinimum(Node<K, V> n) {
        Node<K, V> y = n.parent;
        if (y != null) {
            cut(n, y);
            root.mark = false;
            cascadingRankChange(y);
            root = link(root, n);
        }
    }

    private Option<AddressableHeap.Handle<K, V>> comparatorDeleteMin() {
        if (size == 0) {
            return  Option.empty();
        }
        Node<K, V> z = root;
        Node<K, V> x = root.child;

        // clear fields of previous root
        z.child = null;
        z.next = null;
        z.prev = null;

        // simple case, no children
        if (x == null) {
            root = null;
            size = 0;
            return Option.apply(z);
        }

        // iterate over all children of root
        int maxDegree = -1;
        while (x != null) {
            Node<K, V> nextX = (x.next == x) ? null : x.next;

            // clear parent
            x.parent = null;

            // remove from child list
            x.prev.next = x.next;
            x.next.prev = x.prev;
            x.next = x;
            x.prev = x;

            int d = x.rank;

            while (true) {
                Node<K, V> y = aux[d];
                if (y == null) {
                    break;
                }

                // make sure x's key is smaller
                if (comparator.compare(y.key, x.key) < 0) {
                    Node<K, V> tmp = x;
                    //noinspection SuspiciousNameCombination
                    x = y;
                    y = tmp;
                }

                // make y a child of x
                link(y, x);
                // make link fair by increasing rank
                x.rank++;

                aux[d] = null;
                d++;
            }

            // store result
            aux[d] = x;

            // keep track of max degree
            if (d > maxDegree) {
                maxDegree = d;
            }

            // advance
            x = nextX;
        }

        // recreate tree
        int i = 0;
        while (i <= maxDegree && aux[i] == null) {
            i++;
        }
        root = aux[i];
        aux[i] = null;
        i++;
        while (i <= maxDegree) {
            Node<K, V> n = aux[i];
            if (n != null) {
                if (comparator.compare(n.key, root.key) < 0) {
                    root = link(root, n);
                } else {
                    link(n, root);
                }
                aux[i] = null;
            }
            i++;
        }

        // decrease size
        size--;

        return Option.apply(z);
    }

    /**
     * (unfair) Link y as a child of x.
     */
    private Node<K, V> link(Node<K, V> y, Node<K, V> x) {
        y.parent = x;
        Node<K, V> child = x.child;
        if (child == null) {
            x.child = y;
            y.next = y;
            y.prev = y;
        } else {
            y.prev = child;
            y.next = child.next;
            child.next = y;
            y.next.prev = y;
        }
        return x;
    }

    /**
     * Cut the link between x and its parent y.
     */
    private void cut(Node<K, V> x, Node<K, V> y) {
        // advance y child
        y.child = x.next;
        if (y.child == x) {
            y.child = null;
        }

        // remove x from child list of y
        x.prev.next = x.next;
        x.next.prev = x.prev;
        x.next = x;
        x.prev = x;
        x.parent = null;

        // clear mark
        x.mark = false;

    }

    /*
     * Cascading rank change. Assumes that the root is unmarked.
     */
    private void cascadingRankChange(Node<K, V> y) {
        while (y.mark) {
            y.mark = false;
            if (y.rank > 0) {
                --y.rank;
            }
            y = y.parent;
        }
        y.mark = true;
        if (y.rank > 0) {
            --y.rank;
        }
    }

}
