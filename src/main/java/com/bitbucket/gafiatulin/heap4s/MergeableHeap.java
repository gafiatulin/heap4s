package com.bitbucket.gafiatulin.heap4s;

/**
 * {@inheritDoc}
 *
 * An addressable heap that allows melding with another addressable heap.
 *
 * <p>
 * The second heap becomes empty and unusable after the meld operation, meaning
 * than further insertions are not possible and will throw an
 * {@link IllegalStateException}.
 *
 * <p>
 * A {@link ClassCastException} will be thrown if the two heaps are not of the
 * same type. Moreover, the two heaps need to use the same comparators. If only
 * one of them uses a custom comparator or both use custom comparators but are
 * not the same by <em>equals</em>, an {@code IllegalArgumentException} is
 * thrown.
 *
 * <p>
 * Note that all running time bounds on mergeable heaps are valid assuming that
 * the user does not perform cascading melds on heaps such as:
 *
 * <pre>
 * d.meld(e);
 * c.meld(d);
 * b.meld(c);
 * a.meld(b);
 * </pre>
 *
 * The above scenario, although efficiently supported by using union-find with
 * path compression, invalidates the claimed bounds.
 *
 * @see AddressableHeap
 */

public interface MergeableHeap<K, V> extends AddressableHeap<K, V> {
    /**
     * Meld a heap into the current heap.
     *
     * After the operation the {@code other} heap will be empty and will not
     * permit further insertions.
     *
     * @param other a merge-able heap
     *
     * @throws ClassCastException when {@code other} is not compatible with this heap
     * @throws IllegalArgumentException when {@code other} does not have a compatible comparator
     */
    void meld(MergeableHeap<K, V> other);
}
