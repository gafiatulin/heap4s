package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Option;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;
/**
 * Fibonacci heaps. The heap is sorted by a
 * {@link Comparator} provided at heap creation time.
 *
 * <p>
 * This implementation provides amortized O(1) time for operations that do not
 * involve deleting an element such as {@code insert}, and {@code decreaseKey}.
 * Operation {@code findMin} is worst-case O(1). Operations {@code deleteMin}
 * and {@code delete} are amortized O(log(n)). The operation {@code meld} is
 * also amortized O(1).
 *
 * {@inheritDoc}
 *
 * @see PairingHeap
 * @see SimpleFibonacciHeap
 * @see MergeableHeap
 */

@NotThreadSafe
@TreeBased
public class FibonacciHeap<K, V> implements MergeableHeap<K, V>, Serializable {
    private final static long serialVersionUID = 1;
    /**
     * Size of consolidation auxiliary array. Computed for number of elements
     * equal to {@link Long#MAX_VALUE}.
     */
    private final static int AUX_CONSOLIDATE_ARRAY_SIZE = 91;

    private final Comparator<? super K> comparator;
    private long size;

    /**
     * The root with the minimum key
     */
    private Node<K, V> minRoot;
    /**
     * Number of roots in the root list
     */
    private int roots;

    /**
     * Auxiliary array for consolidation
     */
    private Node<K, V>[] aux;

    /**
     * Used to reference the current heap or some other heap in case of melding,
     * so that handles remain valid even after a meld, without having to iterate
     * over them.
     *
     * In order to avoid maintaining a full-fledged union-find data structure,
     * we disallow a heap to be used in melding more than once. We use however,
     * path-compression in case of cascading melds, that it, a handle moves from
     * one heap to another and then another.
     */
    protected FibonacciHeap<K, V> other;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     */
    @ConstantTime
    @SuppressWarnings("unchecked")
    public FibonacciHeap(Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        this.minRoot = null;
        this.roots = 0;
        this.comparator = comparator;
        this.size = 0;
        this.aux = (Node<K, V>[]) Array.newInstance(Node.class, AUX_CONSOLIDATE_ARRAY_SIZE);
        this.other = this;
    }

    /**
     * {@inheritDoc}
     * @throws IllegalStateException when the heap has already been used in the right hand side of a meld
     */
    @Override
    @ConstantTime(amortized = true)
    public AddressableHeap.Handle<K, V> insert(K key, V value) {
        if (key == null) {
            throw new NullPointerException("Null keys not permitted");
        }
        if (value == null){
            throw new NullPointerException("Null values not permitted");
        }
        if (other != this) {
            throw new IllegalStateException("A heap cannot be used after a meld");
        }
        Node<K, V> n = new Node<>(this, key, value);
        addToRootList(n);
        size++;
        return n;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime(amortized = true)
    public Option<AddressableHeap.Handle<K, V>> findMin() {
        if (size == 0) {
            return Option.empty();
        }
        return Option.apply(minRoot);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime(amortized = true)
    public Option<AddressableHeap.Handle<K, V>> deleteMin() {
        if (size == 0) {
            return Option.empty();
        }
        Node<K, V> z = minRoot;

        // move z children into root list
        Node<K, V> x = z.child;
        while (x != null) {
            Node<K, V> nextX = (x.next == x) ? null : x.next;

            // clear parent
            x.parent = null;

            // remove from child list
            x.prev.next = x.next;
            x.next.prev = x.prev;

            // add to root list
            x.next = minRoot.next;
            x.prev = minRoot;
            minRoot.next = x;
            x.next.prev = x;
            roots++;

            // advance
            x = nextX;
        }
        z.degree = 0;
        z.child = null;

        // remove z from root list
        z.prev.next = z.next;
        z.next.prev = z.prev;
        roots--;

        // decrease size
        size--;

        // update minimum root
        if (z == z.next) {
            minRoot = null;
        } else {
            minRoot = z.next;
            consolidate();
        }

        // clear other fields
        z.next = null;
        z.prev = null;

        return Option.apply(z);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LinearTime
    public Iterator<Handle<K, V>> iterator() {
        // TODO: 22/01/2018
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public long size() {
        return size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public void clear() {
        minRoot = null;
        roots = 0;
        size = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime(amortized = true)
    @SuppressWarnings("unchecked")
    public void meld(MergeableHeap<K, V> other) {
        FibonacciHeap<K, V> h = (FibonacciHeap<K, V>) other;

        // check same comparator
        if (!h.comparator.equals(comparator)) {
            throw new IllegalArgumentException("Cannot meld heaps using different comparators!");
        }

        if (h.other != h) {
            throw new IllegalStateException("A heap cannot be used after a meld.");
        }

        if (size == 0) {
            // copy the other
            minRoot = h.minRoot;
        } else if (h.size != 0) {
            // concatenate root lists
            Node<K, V> h11 = minRoot;
            Node<K, V> h12 = h11.next;
            Node<K, V> h21 = h.minRoot;
            Node<K, V> h22 = h21.next;
            h11.next = h22;
            h22.prev = h11;
            h21.next = h12;
            h12.prev = h21;

            // find new minimum
            if (comparator.compare(h.minRoot.key, minRoot.key) < 0) {
                minRoot = h.minRoot;
            }
        }
        roots += h.roots;
        size += h.size;

        // clear other
        h.size = 0;
        h.minRoot = null;
        h.roots = 0;

        // take ownership
        h.other = this;
    }

    /**
     *  Handle
     */
    static class Node<K, V> implements AddressableHeap.Handle<K, V>, Serializable {
        private final static long serialVersionUID = 1;

        FibonacciHeap<K, V> heap;

        K key;
        V value;
        Node<K, V> parent; // parent
        Node<K, V> child; // any child
        Node<K, V> next; // younger sibling
        Node<K, V> prev; // older sibling
        int degree; // number of children
        boolean mark; // marked or not

        Node(FibonacciHeap<K, V> heap, K key, V value) {
            this.heap = heap;
            this.key = key;
            this.value = value;
            this.parent = null;
            this.child = null;
            this.next = null;
            this.prev = null;
            this.degree = 0;
            this.mark = false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K getKey() {
            return key;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public V getValue() {
            return value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setValue(V value) {
            this.value = value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @LogarithmicTime(amortized = true)
        public boolean delete() {
            if (this.next == null) {
                return false;
            }
            FibonacciHeap<K, V> h = getOwner();
            h.forceDecreaseKeyToMinimum(this);
            h.deleteMin();
            return true;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @ConstantTime(amortized = true)
        public boolean decreaseKey(K newKey) {
            FibonacciHeap<K, V> h = getOwner();
            return h.decreaseKeyWithComparator(this, newKey);
        }

        /*
         * Get the owner heap of the handle. This is union-find with
         * path-compression between heaps.
         */
        private FibonacciHeap<K, V> getOwner() {
            if (heap.other != heap) {
                // find root
                FibonacciHeap<K, V> root = heap;
                while (root != root.other) {
                    root = root.other;
                }
                // path-compression
                FibonacciHeap<K, V> cur = heap;
                while (cur.other != root) {
                    FibonacciHeap<K, V> next = cur.other;
                    cur.other = root;
                    cur = next;
                }
                heap = root;
            }
            return heap;
        }
    }

    /* -------------------------------------------------------------------- */

    /**
     * Decrease the key of a node.
     */
    private boolean decreaseKeyWithComparator(Node<K, V> n, K newKey) {
        int c = comparator.compare(newKey, n.key);
        if (c > 0) {
            return false;
        }
        n.key = newKey;
        if (c == 0) {
            return true;
        }

        if (n.next == null) {
            return false;
        }

        // if not root and heap order violation
        Node<K, V> y = n.parent;
        if (y != null && comparator.compare(n.key, y.key) < 0) {
            cut(n, y);
            cascadingCut(y);
        }

        // update minimum root
        if (comparator.compare(n.key, minRoot.key) < 0) {
            minRoot = n;
        }
        return true;
    }

    /*
     * Decrease the key of a node to the minimum. Helper function for performing
     * a delete operation. Does not change the node's actual key, but behaves as
     * the key is the minimum key in the heap.
     */
    private void forceDecreaseKeyToMinimum(Node<K, V> n) {
        // if not root
        Node<K, V> y = n.parent;
        if (y != null) {
            cut(n, y);
            cascadingCut(y);
        }
        minRoot = n;
    }

    /*
     * Consolidate: Make sure each root tree has a distinct degree.
     */
    @SuppressWarnings("unchecked")
    private void consolidate() {
        int maxDegree = -1;

        // for each node in root list
        int numRoots = roots;
        Node<K, V> x = minRoot;
        while (numRoots > 0) {
            Node<K, V> nextX = x.next;
            int d = x.degree;

            while (true) {
                Node<K, V> y = aux[d];
                if (y == null) {
                    break;
                }

                // make sure x's key is smaller
                int c = comparator.compare(y.key, x.key);
                if (c < 0) {
                    Node<K, V> tmp = x;
                    //noinspection SuspiciousNameCombination
                    x = y;
                    y = tmp;
                }

                // make y a child of x
                link(y, x);

                aux[d] = null;
                d++;
            }

            // store result
            aux[d] = x;

            // keep track of max degree
            if (d > maxDegree) {
                maxDegree = d;
            }

            // advance
            x = nextX;
            numRoots--;
        }

        // recreate root list and find minimum root
        minRoot = null;
        roots = 0;
        for (int i = 0; i <= maxDegree; i++) {
            if (aux[i] != null) {
                addToRootList(aux[i]);
                aux[i] = null;
            }
        }
    }

    /*
     * Remove node y from the root list and make it a child of x. Degree of x
     * increases by 1 and y is unmarked if marked.
     */
    private void link(Node<K, V> y, Node<K, V> x) {
        // remove from root list
        y.prev.next = y.next;
        y.next.prev = y.prev;

        // one less root
        roots--;

        // clear if marked
        y.mark = false;

        // hang as x's child
        x.degree++;
        y.parent = x;

        Node<K, V> child = x.child;
        if (child == null) {
            x.child = y;
            y.next = y;
            y.prev = y;
        } else {
            y.prev = child;
            y.next = child.next;
            child.next = y;
            y.next.prev = y;
        }
    }

    /*
     * Cut the link between x and its parent y making x a root.
     */
    private void cut(Node<K, V> x, Node<K, V> y) {
        // remove x from child list of y
        x.prev.next = x.next;
        x.next.prev = x.prev;
        y.degree--;
        if (y.degree == 0) {
            y.child = null;
        } else if (y.child == x) {
            y.child = x.next;
        }

        // add x to the root list
        x.parent = null;
        addToRootList(x);

        // clear if marked
        x.mark = false;
    }

    /*
     * Cascading cut until a root or an unmarked node is found.
     */
    private void cascadingCut(Node<K, V> y) {
        Node<K, V> z;
        while ((z = y.parent) != null) {
            if (!y.mark) {
                y.mark = true;
                break;
            }
            //noinspection SuspiciousNameCombination
            cut(y, z);
            y = z;
        }
    }

    /*
     * Add a node to the root list and update the minimum.
     */
    @SuppressWarnings("unchecked")
    private void addToRootList(Node<K, V> n) {
        if (minRoot == null) {
            n.next = n;
            n.prev = n;
            minRoot = n;
            roots = 1;
        } else {
            n.next = minRoot.next;
            n.prev = minRoot;
            minRoot.next.prev = n;
            minRoot.next = n;
            int c = comparator.compare(n.key, minRoot.key);
            if (c < 0) {
                minRoot = n;
            }
            roots++;
        }
    }
}
