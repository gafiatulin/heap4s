package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Option;

import java.io.Serializable;
import java.util.*;

/**
 * Pairing heaps. The heap is sorted by a
 * {@link Comparator} provided at heap creation time.
 *
 * <p>
 * This implementation provides amortized O(log(n)) time cost for the
 * {@code insert}, {@code deleteMin}, and {@code decreaseKey} operations.
 * Operation {@code findMin}, is a worst-case O(1) operation. The algorithms are
 * based on the <a href="http://dx.doi.org/10.1007/BF01840439">pairing heap
 * paper</a>. Pairing heaps are very efficient in practice, especially in
 * applications requiring the {@code decreaseKey} operation. The operation
 * {@code meld} is amortized O(log(n)).
 *
 * {@inheritDoc}
 *
 * @see FibonacciHeap
 * @see MergeableHeap
 */

@NotThreadSafe
@TreeBased
public class PairingHeap<K, V> implements MergeableHeap<K, V>, Serializable {
    private final static long serialVersionUID = 1;

    private final Comparator<? super K> comparator;
    private long size;
    private Node<K, V> root;

    /**
     * Used to reference the current heap or some other pairing heap in case of
     * melding, so that handles remain valid even after a meld, without having
     * to iterate over them.
     *
     * In order to avoid maintaining a full-fledged union-find data structure,
     * we disallow a heap to be used in melding more than once. We use however,
     * path-compression in case of cascading melds, that it, a handle moves from
     * one heap to another and then another.
     */
    private PairingHeap<K, V> other;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     */
    @ConstantTime
    public PairingHeap(Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        this.root = null;
        this.comparator = comparator;
        this.size = 0;
        this.other = this;
    }

    /**
     * {@inheritDoc}
     * @throws IllegalStateException when the heap has already been used in the right hand side of a meld
     */
    @Override
    @LogarithmicTime(amortized = true)
    public AddressableHeap.Handle<K, V> insert(K key, V value) {
        if (key == null) {
            throw new NullPointerException("Null keys not permitted");
        }
        if (value == null){
            throw new NullPointerException("Null values not permitted");
        }
        if (other != this) {
            throw new IllegalStateException("A heap cannot be used after a meld");
        }
        Node<K, V> n = new Node<K, V>(this, key, value);

        root = linkWithComparator(root, n);

        size++;
        return n;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime()
    public Option<AddressableHeap.Handle<K, V>> findMin() {
        if (size == 0) {
            return Option.empty();
        }
        return Option.apply(root);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime(amortized = true)
    public Option<AddressableHeap.Handle<K, V>> deleteMin() {
        if (size == 0) {
            return Option.empty();
        }

        Handle<K, V> oldRoot = root;

        // cut all children, combine them and overwrite old root
        root = combine(cutChildren(root));

        // decrease size
        size--;

        return Option.apply(oldRoot);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LinearTime
    @SuppressWarnings("unchecked")
    public Iterator<Handle<K, V>> iterator() {
        return new Iterator<Handle<K, V>>() {
            private Node current = root;
            private Deque<Node> stack = new ArrayDeque<>();
            public boolean hasNext() {
                return current != null;
            }
            public Handle<K, V> next() {
                Node previous = current;
                if(current.olderChild!= null){
                    stack.push(current);
                    current = current.olderChild;
                } else if(current.youngerSibling!=null){
                    current = current.youngerSibling;
                } else{
                    while (current.youngerSibling == null && !stack.isEmpty()){
                        current = stack.pop();
                    }
                    if(current.youngerSibling != null){
                        current = current.youngerSibling;
                    } else {
                        current = current.olderSiblingOrParent;
                    }
                }
                return previous;
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public long size() {
        return size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime()
    public void clear() {
        root = null;
        size = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime(amortized = true)
    public void meld(MergeableHeap<K, V> other) {
        PairingHeap<K, V> h = (PairingHeap<K, V>) other;

        // check same comparator

        if (!h.comparator.equals(comparator)) {
            throw new IllegalArgumentException("Cannot meld heaps using different comparators!");
        }

        if (h.other != h) {
            throw new IllegalStateException("A heap cannot be used after a meld.");
        }

        // perform the meld
        size += h.size;
        root = linkWithComparator(root, h.root);

        // clear other
        h.size = 0;
        h.root = null;

        // take ownership
        h.other = this;
    }

    /**
     *  Handle
     */
    static class Node<K, V> implements AddressableHeap.Handle<K, V>, Serializable {

        private final static long serialVersionUID = 1;

        PairingHeap<K, V> heap;

        K key;
        V value;
        Node<K, V> olderChild; // older child
        Node<K, V> youngerSibling; // younger sibling
        Node<K, V> olderSiblingOrParent; // older sibling or parent

        Node(PairingHeap<K, V> heap, K key, V value) {
            this.heap = heap;
            this.key = key;
            this.value = value;
            this.olderChild = null;
            this.youngerSibling = null;
            this.olderSiblingOrParent = null;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K getKey() {
            return key;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public V getValue() {
            return value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setValue(V value) {
            this.value = value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @LogarithmicTime(amortized = true)
        public boolean decreaseKey(K newKey) {
            return getOwner().decreaseKey(this, newKey);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @LogarithmicTime(amortized = true)
        public boolean delete() {
            return getOwner().delete(this);
        }

        /**
         * Get the owner heap of the handle. This is union-find with path-compression between heaps.
         */
        private PairingHeap<K, V> getOwner() {
            if (heap.other != heap) {
                // find root
                PairingHeap<K, V> root = heap;
                while (root != root.other) {
                    root = root.other;
                }
                // path-compression
                PairingHeap<K, V> cur = heap;
                while (cur.other != root) {
                    PairingHeap<K, V> next = cur.other;
                    cur.other = root;
                    cur = next;
                }
                heap = root;
            }
            return heap;
        }
    }

    /* -------------------------------------------------------------------- */

    /**
     * Decrease the key of a node.
     *
     * @param n the node
     * @param newKey the new key
     */
    @SuppressWarnings("unchecked")
    private boolean decreaseKey(Node<K, V> n, K newKey) {
        int c = comparator.compare(newKey, n.key);

        if (c > 0) {
            return false;
        }
        n.key = newKey;
        if (c == 0 || root == n) {
            return true;
        }

        if (n.olderSiblingOrParent == null) {
            return false;
        }

        // unlink from parent
        if (n.youngerSibling != null) {
            n.youngerSibling.olderSiblingOrParent = n.olderSiblingOrParent;
        }
        if (n.olderSiblingOrParent.olderChild == n) { // I am the oldest :(
            n.olderSiblingOrParent.olderChild = n.youngerSibling;
        } else { // I have an older sibling!
            n.olderSiblingOrParent.youngerSibling = n.youngerSibling;
        }
        n.youngerSibling = null;
        n.olderSiblingOrParent = null;

        // merge with root
        root = linkWithComparator(root, n);
        return true;
    }

    /**
     * Delete a node
     */
    private boolean delete(Node<K, V> n) {
        if (root == n) {
            deleteMin();
            n.olderChild = null;
            n.youngerSibling = null;
            n.olderSiblingOrParent = null;
            return true;
        }

        if (n.olderSiblingOrParent == null) {
            return false;
        }

        // unlink from parent
        if (n.youngerSibling != null) {
            n.youngerSibling.olderSiblingOrParent = n.olderSiblingOrParent;
        }
        if (n.olderSiblingOrParent.olderChild == n) { // I am the oldest :(
            n.olderSiblingOrParent.olderChild = n.youngerSibling;
        } else { // I have an older sibling!
            n.olderSiblingOrParent.youngerSibling = n.youngerSibling;
        }
        n.youngerSibling = null;
        n.olderSiblingOrParent = null;

        // perform delete-min at tree rooted at this
        Node<K, V> t = combine(cutChildren(n));

        // and merge with other cut tree
        root = linkWithComparator(root, t);

        size--;
        return true;
    }

    /**
     * Two pass pair and compute root.
     */
    private Node<K, V> combine(Node<K, V> l) {
        if (l == null) {
            return null;
        }

        assert l.olderSiblingOrParent == null;

        // left-right pass
        Node<K, V> pairs = null;
        Node<K, V> it = l, p_it;
        while (it != null) {
            p_it = it;
            it = it.youngerSibling;

            if (it == null) {
                // append last node to pair list
                p_it.youngerSibling = pairs;
                p_it.olderSiblingOrParent = null;
                pairs = p_it;
            } else {
                Node<K, V> n_it = it.youngerSibling;

                // disconnect both
                p_it.youngerSibling = null;
                p_it.olderSiblingOrParent = null;
                it.youngerSibling = null;
                it.olderSiblingOrParent = null;

                // link trees
                p_it = linkWithComparator(p_it, it);

                // append to pair list
                p_it.youngerSibling = pairs;
                pairs = p_it;

                // advance
                it = n_it;
            }
        }

        // second pass (reverse order - due to add first)
        it = pairs;
        Node<K, V> f = null;
        while (it != null) {
            Node<K, V> nextIt = it.youngerSibling;
            it.youngerSibling = null;
            f = linkWithComparator(f, it);
            it = nextIt;
        }

        return f;
    }

    /**
     * Cut the children of a node and return the list.
     *
     * @param n the node
     *
     * @return the first node in the children list
     */
    private Node<K, V> cutChildren(Node<K, V> n) {
        Node<K, V> child = n.olderChild;
        n.olderChild = null;
        if (child != null) {
            child.olderSiblingOrParent = null;
        }
        return child;
    }

    private Node<K, V> linkWithComparator(Node<K, V> f, Node<K, V> s) {
        if (s == null) {
            return f;
        } else if (f == null) {
            return s;
        } else if (comparator.compare(f.key, s.key) <= 0) {
            s.youngerSibling = f.olderChild;
            s.olderSiblingOrParent = f;
            if (f.olderChild != null) {
                f.olderChild.olderSiblingOrParent = s;
            }
            f.olderChild = s;
            return f;
        } else {
            return linkWithComparator(s, f);
        }
    }
}
