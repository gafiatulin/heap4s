package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Tuple2;

import java.io.Serializable;
import java.util.*;

/**
 * An array based binary addressable heap. The heap is sorted by a
 * {@link Comparator} provided at heap creation time.
 *
 * Constructing such a heap from an array of elements can be performed using the
 * method {@link #heapify(Object[], Object[], Comparator)} or
 * {@link #heapify(Tuple2[], Comparator)} in linear time.
 * <p>
 *
 * {@inheritDoc}
 *
 * @see AbstractArrayHeap
 */
@NotThreadSafe
@ArrayBased
public class BinaryArrayHeap<K, V> extends AbstractArrayHeap<K, V> implements Serializable {
    private final static long serialVersionUID = 1;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     *
     * <p>
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * <p>
     * The initial capacity of the heap is {@link #DEFAULT_HEAP_CAPACITY} and
     * adjusts automatically based on the sequence of insertions and deletions.
     *
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     */
    public BinaryArrayHeap(Comparator<? super K> comparator) {
        this(comparator, DEFAULT_HEAP_CAPACITY);
    }

    /**
     * Constructs a new, empty heap, with a provided initial capacity ordered
     * according to the given comparator.
     *
     * <p>
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * <p>
     * The initial capacity of the heap is provided by the user and is adjusted
     * automatically based on the sequence of insertions and deletions. The
     * capacity will never become smaller than the initial requested capacity.
     *
     * @param comparator the comparator that will be used to order this heap.
     * @param capacity the initial heap capacity
     *
     * @throws NullPointerException when {@code comparator} is null
     * @throws IllegalArgumentException when @{code capacity < 0 || capacity > (Integer.MAX_VALUE - 8 - 1)}
     */
    public BinaryArrayHeap(Comparator<? super K> comparator, int capacity) {
        super(comparator, capacity);
    }

    /**
     * Create a heap from an array of elements. The elements of the array are
     * not destroyed. The method has linear time complexity.
     *
     * @param <K> the type of keys maintained by the heap
     * @param <V> the type of values maintained by the heap
     * @param keys an array of keys
     * @param values an array of values
     * @param comparator the comparator to use
     *
     * @throws NullPointerException when {@code comparator} or {@code keys} or {@code values} is null
     * @throws IllegalArgumentException when {@code keys} and {@code values} have different sizes
     */
    @LinearTime
    public static <K, V> BinaryArrayHeap<K, V> heapify(K[] keys, V[] values, Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        if (keys == null) {
            throw new NullPointerException("Keys array cannot be null");
        }
        if(values == null){
            throw new NullPointerException("Values array cannot be null");
        }
        if (keys.length != values.length) {
            throw new IllegalArgumentException("Values array must have the same length as the keys array");
        }
        if (keys.length == 0) {
            return new BinaryArrayHeap<>(comparator);
        }

        BinaryArrayHeap<K, V> h = new BinaryArrayHeap<>(comparator, keys.length);

        for (int i = 0; i < keys.length; i++) {
            K key = keys[i];
            V value = values[i];
            AbstractArrayHeap<K, V>.ArrayHandle ah = h.new ArrayHandle(key, value);
            ah.index = i + 1;
            h.underlyingArray[i + 1] = ah;
        }
        h.size = keys.length;

        for (int i = keys.length / 2; i > 0; i--) {
            h.fixdownWithComparator(i);
        }

        return h;
    }

    /**
     * Create a heap from an array of elements. The elements of the array are
     * not destroyed. The method has linear time complexity.
     *
     * @param <K> the type of keys maintained by the heap
     * @param <V> the type of values maintained by the heap
     * @param kvs an array of key-value pairs
     * @param comparator the comparator to use
     *
     * @throws NullPointerException when {@code comparator} or {@code cvs} is null
     */
    public static <K, V> BinaryArrayHeap<K, V> heapify(Tuple2<K, V>[] kvs, Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        if (kvs == null) {
            throw new NullPointerException("Key-value array cannot be null");
        }
        if (kvs.length == 0) {
            return new BinaryArrayHeap<>(comparator);
        }

        BinaryArrayHeap<K, V> h = new BinaryArrayHeap<>(comparator, kvs.length);

        for (int i = 0; i < kvs.length; i++) {
            Tuple2<K, V> kv = kvs[i];
            AbstractArrayHeap<K, V>.ArrayHandle ah = h.new ArrayHandle(kv._1, kv._2);
            ah.index = i + 1;
            h.underlyingArray[i + 1] = ah;
        }
        h.size = kvs.length;

        for (int i = kvs.length / 2; i > 0; i--) {
            h.fixdownWithComparator(i);
        }

        return h;
    }

    @Override
    protected void forceFixup(int k) {
        ArrayHandle h = underlyingArray[k];
        while (k > 1) {
            underlyingArray[k] = underlyingArray[k / 2];
            underlyingArray[k].index = k;
            k /= 2;
        }
        underlyingArray[k] = h;
        h.index = k;
    }

    @Override
    protected void fixupWithComparator(int k) {
        ArrayHandle h = underlyingArray[k];
        while (k > 1 && comparator.compare(underlyingArray[k / 2].getKey(), h.getKey()) > 0) {
            underlyingArray[k] = underlyingArray[k / 2];
            underlyingArray[k].index = k;
            k /= 2;
        }
        underlyingArray[k] = h;
        h.index = k;
    }

    @Override
    protected void fixdownWithComparator(int k) {
        ArrayHandle h = underlyingArray[k];
        while (2 * k <= size) {
            int j = 2 * k;
            if (j < size && comparator.compare(underlyingArray[j].getKey(), underlyingArray[j + 1].getKey()) > 0) {
                j++;
            }
            if (comparator.compare(h.getKey(), underlyingArray[j].getKey()) <= 0) {
                break;
            }
            underlyingArray[k] = underlyingArray[j];
            underlyingArray[k].index = k;
            k = j;
        }
        underlyingArray[k] = h;
        h.index = k;
    }
}
