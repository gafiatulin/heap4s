package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Option;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.*;

/**
 * The costless meld variant of the pairing heaps. The heap is sorted by a
 * {@link Comparator} provided at heap creation time.
 *
 * <p>
 * This implementation provides amortized O(1) time for {@code findMin} and
 * {@code insert}, amortized O(log(n)) for {@code deleteMin} and {@code delete}
 * and amortized O(loglog(n)) for the {@code decreaseKey} operation. The
 * operation {@code meld} takes amortized zero time.
 * <p>
 *
 * This variant of the pairing heap is due to Amr Elmasry, described in detail
 * in the following
 * <a href="http://dx.doi.org/10.1007/978-3-642-15781-3_16">paper</a>:
 * <ul><li>Amr Elmasry, Pairing Heaps with Costless Meld, In Proceedings of the 18th
 * Annual European Symposium on Algorithms (ESA 2010), 183--193, 2010.</li></ul>
 *
 * {@inheritDoc}
 *
 * @see PairingHeap
 * @see FibonacciHeap
 * @see MergeableHeap
 */

@NotThreadSafe
@TreeBased
public class CostlessMeldPairingHeap<K, V> implements MergeableHeap<K, V>, Serializable{
    private final static long serialVersionUID = 1;

    /**
     * Maximum length of decrease pool for long type.
     */
    private final static int DEFAULT_DECREASE_POOL_SIZE = 64 + 1;

    private final Comparator<? super K> comparator;
    private long size;
    private Node<K, V> root;

    /**
     * The decrease pool
     */
    private Node<K, V>[] decreasePool;

    /**
     * How many elements are valid in the decrease pool
     */
    private byte decreasePoolSize;

    /**
     * Index of node with minimum key in the decrease pool. Not existent if
     * {@literal decreasePoolMin >= decreasePoolSize}.
     */
    private byte decreasePoolMinPos;

    /**
     * Comparator for nodes in the decrease pool. Initialized lazily and used
     * when sorting entries in the decrease pool.
     */
    private transient Comparator<Node<K, V>> decreasePoolComparator;

    /**
     * Used to reference the current heap or some other pairing heap in case of
     * melding, so that handles remain valid even after a meld, without having
     * to iterate over them.
     *
     * In order to avoid maintaining a full-fledged union-find data structure,
     * we disallow a heap to be used in melding more than once. We use however,
     * path-compression in case of cascading melds, that it, a handle moves from
     * one heap to another and then another.
     */
    private CostlessMeldPairingHeap<K, V> other;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     *
     * <p>
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     */
    @ConstantTime
    @SuppressWarnings("unchecked")
    public CostlessMeldPairingHeap(Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        this.decreasePool = (Node<K, V>[]) Array.newInstance(Node.class, DEFAULT_DECREASE_POOL_SIZE);
        this.decreasePoolSize = 0;
        this.decreasePoolMinPos = 0;
        this.comparator = comparator;
        this.decreasePoolComparator = null;
        this.other = this;
    }

    /**
     * {@inheritDoc}
     * @throws IllegalStateException when the heap has already been used in the right hand side of a meld
     */
    @Override
    @LogarithmicTime(amortized = true)
    public Handle<K, V> insert(K key, V value) {
        if (key == null) {
            throw new NullPointerException("Null keys not permitted");
        }
        if (value == null){
            throw new NullPointerException("Null values not permitted");
        }
        if (other != this) {
            throw new IllegalStateException("A heap cannot be used after a meld");
        }
        Node<K, V> n = new Node<>(this, key, value);
        root = linkWithComparator(root, n);
        size++;
        return n;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    @ConstantTime
    public Option<Handle<K, V>> findMin() {
        if (size == 0) {
            return Option.empty();
        } else if (decreasePoolMinPos >= decreasePoolSize) {
            return Option.apply(root);
        } else {
            Node<K, V> poolMin = decreasePool[decreasePoolMinPos];
            int c = comparator.compare(root.key, poolMin.key);
            if (c <= 0) {
                return Option.apply(root);
            } else {
                return Option.apply(poolMin);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    @LogarithmicTime(amortized = true)
    public Option<AddressableHeap.Handle<K, V>> deleteMin() {
        if (size == 0) {
            return Option.empty();
        }

        Node<K, V> min;
        if (decreasePoolMinPos >= decreasePoolSize) {
            // decrease pool empty
            min = root;

            // cut all children, and combine them
            root = combine(cutChildren(root));
        } else {
            Node<K, V> poolMin = decreasePool[decreasePoolMinPos];
            int c = comparator.compare(root.key, poolMin.key);

            if (c <= 0) {
                // root is smaller
                min = root;

                // cut children, combine
                Node<K, V> childrenTree = combine(cutChildren(root));
                root = null;

                // Append to decrease pool without updating minimum as we are going to consolidate anyway
                if (childrenTree != null) {
                    addPool(childrenTree, false);
                }
                consolidate();
            } else {
                // minimum in pool is smaller
                min = poolMin;

                // cut children, combine
                Node<K, V> childrenTree = combine(cutChildren(poolMin));

                if (childrenTree != null) {
                    // add to location of previous minimum and consolidate
                    decreasePool[decreasePoolMinPos] = childrenTree;
                    childrenTree.poolIndex = decreasePoolMinPos;
                } else {
                    decreasePool[decreasePoolMinPos] = decreasePool[decreasePoolSize - 1];
                    decreasePool[decreasePoolMinPos].poolIndex = decreasePoolMinPos;
                    decreasePool[decreasePoolSize - 1] = null;
                    decreasePoolSize--;
                }
                poolMin.poolIndex = Node.NO_INDEX;

                consolidate();
            }
        }

        size--;
        return Option.apply(min);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LinearTime
    @SuppressWarnings("unchecked")
    public Iterator<Handle<K, V>> iterator() {
        return new Iterator<Handle<K, V>>() {
            private Node current = root;
            private Deque<Node> stack = new ArrayDeque<>();
            public boolean hasNext() {
                return current != null;
            }
            public Handle<K, V> next() {
                Node previous = current;
                if(current.olderChild!= null){
                    stack.push(current);
                    current = current.olderChild;
                } else if(current.youngerSibling!=null){
                    current = current.youngerSibling;
                } else{
                    while (current.youngerSibling == null && !stack.isEmpty()){
                        current = stack.pop();
                    }
                    if(current.youngerSibling != null){
                        current = current.youngerSibling;
                    } else {
                        current = current.olderSiblingOrParent;
                    }
                }
                return previous;
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public long size() {
        return size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @SuppressWarnings("unchecked")
    @ConstantTime
    public void clear() {
        root = null;
        size = 0;
        decreasePool = (Node[]) Array.newInstance(Node.class, DEFAULT_DECREASE_POOL_SIZE);
        decreasePoolSize = 0;
        decreasePoolMinPos = 0;
    }

    /**
     * {@inheritDoc}
     *
     * This operation takes amortized zero cost.
     */
    @Override
    @ConstantTime(amortized = true)
    public void meld(MergeableHeap<K, V> other) {
        CostlessMeldPairingHeap<K, V> h = (CostlessMeldPairingHeap<K, V>) other;

        // check same comparator
        if (!h.comparator.equals(comparator)) {
            throw new IllegalArgumentException("Cannot meld heaps using different comparators!");
        }

        if (h.other != h) {
            throw new IllegalStateException("A heap cannot be used after a meld.");
        }

        // meld
        if (size < h.size) {
            consolidate();

            root = linkWithComparator(h.root, root);

            decreasePoolSize = h.decreasePoolSize;
            h.decreasePoolSize = 0;

            decreasePoolMinPos = h.decreasePoolMinPos;
            h.decreasePoolMinPos = 0;

            Node<K, V>[] tmp = decreasePool;
            decreasePool = h.decreasePool;
            h.decreasePool = tmp;
        } else {
            h.consolidate();
            root = linkWithComparator(h.root, root);
        }

        size += h.size;
        h.root = null;
        h.size = 0;

        // take ownership
        h.other = this;
    }

    /**
     *  Handle
     */
    static class Node<K, V> implements AddressableHeap.Handle<K, V>, Serializable {
        private final static long serialVersionUID = 1;
        static final byte NO_INDEX = -1;

        CostlessMeldPairingHeap<K, V> heap;

        K key;
        V value;
        Node<K, V> olderChild; // older child
        Node<K, V> youngerSibling; // younger sibling
        Node<K, V> olderSiblingOrParent; // older sibling or parent
        byte poolIndex; // position in decrease pool

        Node(CostlessMeldPairingHeap<K, V> heap, K key, V value) {
            this.heap = heap;
            this.key = key;
            this.value = value;
            this.olderChild = null;
            this.youngerSibling = null;
            this.olderSiblingOrParent = null;
            this.poolIndex = NO_INDEX;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public K getKey() {
            return key;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public V getValue() {
            return value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void setValue(V value) {
            this.value = value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @LogLogTime(amortized = true)
        public boolean delete() {
            return getOwner().delete(this);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        @LogLogTime(amortized = true)
        public boolean decreaseKey(K newKey) {
            return getOwner().decreaseKey(this, newKey);
        }

        /**
         * Get the owner heap of the handle. This is union-find with
         * path-compression between heaps.
         */
        private CostlessMeldPairingHeap<K, V> getOwner() {
            if (heap.other != heap) {
                // find root
                CostlessMeldPairingHeap<K, V> root = heap;
                while (root != root.other) {
                    root = root.other;
                }
                // path-compression
                CostlessMeldPairingHeap<K, V> cur = heap;
                while (cur.other != root) {
                    CostlessMeldPairingHeap<K, V> next = cur.other;
                    cur.other = root;
                    cur = next;
                }
                heap = root;
            }
            return heap;
        }
    }

    /* -------------------------------------------------------------------- */

    /**
     * Delete a node.
     *
     * @param n the node
     */
    private boolean delete(Node<K, V> n) {
        if (n != root && n.olderSiblingOrParent == null && n.poolIndex == Node.NO_INDEX) {
            // no root, no parent and no pool index
            return false;
        }

        // node has a parent
        if (n.olderSiblingOrParent != null) {
            // cut oldest child
            Node<K, V> oldestChild = cutOldestChild(n);
            if (oldestChild != null) {
                linkInPlace(oldestChild, n);
            } else {
                cutFromParent(n);
            }
        }

        // node has no parent
        // cut children
        Node<K, V> childrenTree = combine(cutChildren(n));
        boolean checkConsolidate = false;
        if (childrenTree != null) {
            checkConsolidate = true;
            addPool(childrenTree, true);
        }

        size--;
        if (n == root) {
            root = null;
            consolidate();
            checkConsolidate = false;
        } else if (n.poolIndex != Node.NO_INDEX) {
            byte curIndex = n.poolIndex;
            decreasePool[curIndex] = decreasePool[decreasePoolSize - 1];
            decreasePool[curIndex].poolIndex = curIndex;
            decreasePool[decreasePoolSize - 1] = null;
            decreasePoolSize--;
            n.poolIndex = Node.NO_INDEX;
            if (curIndex == decreasePoolMinPos) {
                // in decrease pool, and also the minimum
                consolidate();
                checkConsolidate = false;
            } else {
                // in decrease pool, and not the minimum
                if (decreasePoolMinPos == decreasePoolSize) {
                    decreasePoolMinPos = curIndex;
                }
                checkConsolidate = true;
            }
        }

        // if decrease pool has >= ceil(logn) trees, consolidate
        if (checkConsolidate) {
            double sizeAsDouble = size;
            if (decreasePoolSize >= Math.getExponent(sizeAsDouble) + 1) {
                consolidate();
            }
        }
        return true;
    }

    /**
     * Decrease the key of a node.
     */

    @SuppressWarnings("unchecked")
    private boolean decreaseKey(Node<K, V> n, K newKey) {
        int c = comparator.compare(newKey, n.key);

        if (c > 0) {
            return false;
        }
        n.key = newKey;

        if (c == 0 || root == n) {
            // root or no change in key
            return true;
        } else if (n.olderSiblingOrParent == null && n.poolIndex == Node.NO_INDEX) {
            // no root, no parent and no pool index
            return false;
        } else if (n.olderSiblingOrParent == null) {
            // no parent and not root, so inside pool
            Node<K, V> poolMin = decreasePool[decreasePoolMinPos];
            c = comparator.compare(newKey, poolMin.key);
            if (c < 0) {
                decreasePoolMinPos = n.poolIndex;
            }
            return true;
        } else {
            // node has a parent
            Node<K, V> oldestChild = cutOldestChild(n);
            if (oldestChild != null) {
                linkInPlace(oldestChild, n);
            } else {
                cutFromParent(n);
            }

            // append node (minus oldest child) to decrease pool
            addPool(n, true);

            // if decrease pool has >= ceil(logn) trees, consolidate
            double sizeAsDouble = size;
            if (decreasePoolSize >= Math.getExponent(sizeAsDouble) + 1) {
                consolidate();
            }
            return true;
        }
    }


    /**
     * Consolidate. Combine the trees of the decrease pool in one tree by
     * sorting the values of the roots of these trees, and linking the trees in
     * this order such that their roots form a path of nodes in the combined
     * tree (make every root the leftmost child of the root with the next
     * smaller value). Join this combined tree with the main tree.
     */
    @SuppressWarnings("unchecked")
    private void consolidate() {
        if (decreasePoolSize == 0) {
            return;
        }
        // lazily initialize comparator
        if (decreasePoolComparator == null) {
            decreasePoolComparator = (o1, o2) -> comparator.compare(o1.key, o2.key);
        }
        // sort
        Arrays.sort(decreasePool, 0, decreasePoolSize, decreasePoolComparator);
        int i = decreasePoolSize - 1;
        Node<K, V> s = decreasePool[i];
        s.poolIndex = Node.NO_INDEX;
        while (i > 0) {
            Node<K, V> f = decreasePool[i - 1];
            f.poolIndex = Node.NO_INDEX;
            decreasePool[i] = null;
            // link (no comparison, due to sort)
            s.youngerSibling = f.olderChild;
            s.olderSiblingOrParent = f;
            if (f.olderChild != null) {
                f.olderChild.olderSiblingOrParent = s;
            }
            f.olderChild = s;
            // advance
            s = f;
            i--;
        }
        // empty decrease pool
        decreasePool[0] = null;
        decreasePoolSize = 0;
        decreasePoolMinPos = 0;
        // merge tree with root
        root = linkWithComparator(root, s);
    }

    /**
     * Append to decrease pool.
     */
    @SuppressWarnings("unchecked")
    private void addPool(Node<K, V> n, boolean updateMinimum) {
        decreasePool[decreasePoolSize] = n;
        n.poolIndex = decreasePoolSize;
        decreasePoolSize++;
        if (updateMinimum && decreasePoolSize > 1) {
            Node<K, V> poolMin = decreasePool[decreasePoolMinPos];
            int c = comparator.compare(n.key, poolMin.key);
            if (c < 0) {
                decreasePoolMinPos = n.poolIndex;
            }
        }
    }

    /**
     * Two pass pair and compute root.
     */
    private Node<K, V> combine(Node<K, V> l) {
        if (l == null) {
            return null;
        }
        assert l.olderSiblingOrParent == null;
        // left-right pass
        Node<K, V> pairs = null;
        Node<K, V> it = l, p_it;
        while (it != null) {
            p_it = it;
            it = it.youngerSibling;
            if (it == null) {
                // append last node to pair list
                p_it.youngerSibling = pairs;
                p_it.olderSiblingOrParent = null;
                pairs = p_it;
            } else {
                Node<K, V> n_it = it.youngerSibling;
                // disconnect both
                p_it.youngerSibling = null;
                p_it.olderSiblingOrParent = null;
                it.youngerSibling = null;
                it.olderSiblingOrParent = null;
                // link trees
                p_it = linkWithComparator(p_it, it);
                // append to pair list
                p_it.youngerSibling = pairs;
                pairs = p_it;
                // advance
                it = n_it;
            }
        }
        // second pass (reverse order - due to add first)
        it = pairs;
        Node<K, V> f = null;
        while (it != null) {
            Node<K, V> nextIt = it.youngerSibling;
            it.youngerSibling = null;
            f = linkWithComparator(f, it);
            it = nextIt;
        }
        return f;
    }

    /**
     * Cut the children of a node and return the list.
     *
     * @param n the node
     * @return the first node in the children list
     */
    private Node<K, V> cutChildren(Node<K, V> n) {
        Node<K, V> child = n.olderChild;
        n.olderChild = null;
        if (child != null) {
            child.olderSiblingOrParent = null;
        }
        return child;
    }

    /**
     * Cut the oldest child of a node.
     *
     * @param n the node
     * @return the oldest child of a node or null
     */
    private Node<K, V> cutOldestChild(Node<K, V> n) {
        Node<K, V> oldestChild = n.olderChild;
        if (oldestChild != null) {
            if (oldestChild.youngerSibling != null) {
                oldestChild.youngerSibling.olderSiblingOrParent = n;
            }
            n.olderChild = oldestChild.youngerSibling;
            oldestChild.youngerSibling = null;
            oldestChild.olderSiblingOrParent = null;
        }
        return oldestChild;
    }

    /**
     * Cut a node from its parent.
     *
     * @param n the node
     */
    private void cutFromParent(Node<K, V> n) {
        if (n.olderSiblingOrParent != null) {
            if (n.youngerSibling != null) {
                n.youngerSibling.olderSiblingOrParent = n.olderSiblingOrParent;
            }
            if (n.olderSiblingOrParent.olderChild == n) { // I am the oldest :(
                n.olderSiblingOrParent.olderChild = n.youngerSibling;
            } else { // I have an older sibling!
                n.olderSiblingOrParent.youngerSibling = n.youngerSibling;
            }
            n.youngerSibling = null;
            n.olderSiblingOrParent = null;
        }
    }

    /**
     * Put an orphan node into the position of another node. The other node
     * becomes an orphan.
     *
     * @param orphan the orphan node
     * @param n the node which will become an orphan
     */
    private void linkInPlace(Node<K, V> orphan, Node<K, V> n) {
        // link orphan at node's position
        orphan.youngerSibling = n.youngerSibling;
        if (n.youngerSibling != null) {
            n.youngerSibling.olderSiblingOrParent = orphan;
        }
        orphan.olderSiblingOrParent = n.olderSiblingOrParent;
        if (n.olderSiblingOrParent != null) {
            if (n.olderSiblingOrParent.olderChild == n) { // node is the oldest :(
                n.olderSiblingOrParent.olderChild = orphan;
            } else { // node has an older sibling!
                n.olderSiblingOrParent.youngerSibling = orphan;
            }
        }
        n.olderSiblingOrParent = null;
        n.youngerSibling = null;
    }

    private Node<K, V> linkWithComparator(Node<K, V> f, Node<K, V> s) {
        if (s == null) {
            return f;
        } else if (f == null) {
            return s;
        } else if (comparator.compare(f.key, s.key) <= 0) {
            s.youngerSibling = f.olderChild;
            s.olderSiblingOrParent = f;
            if (f.olderChild != null) {
                f.olderChild.olderSiblingOrParent = s;
            }
            f.olderChild = s;
            return f;
        } else {
            return linkWithComparator(s, f);
        }
    }
}
