package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Option;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Implementation of a heap using an array representation.
 *
 * <p>
 * The implementation uses an array in order to store the elements and
 * automatically maintains the size of the array much like a
 * {@link java.util.Vector} does, providing amortized O(log(n)) time cost for
 * the {@code insert} and {@code deleteMin} operations. Operation
 * {@code findMin}, is a worst-case O(1) operation. Operations {@code delete}
 * and {@code decreaseKey} take worst-case O(log(n)) time. The bounds are
 * worst-case if the user initializes the heap with a capacity larger or equal
 * to the total number of elements that are going to be inserted into the heap.
 *
 * {@inheritDoc}
 *
 * @see AddressableHeap
 */

@NotThreadSafe
@ArrayBased
public abstract class AbstractArrayHeap<K, V> implements AddressableHeap<K, V>, Serializable {
    private static final long serialVersionUID = 1L;

    protected static final int DEFAULT_HEAP_CAPACITY = 16;
    protected static final int NO_INDEX = -1;
    protected static final int MAX_HEAP_CAPACITY = Integer.MAX_VALUE - 8 - 1;
    protected static final int MIN_HEAP_CAPACITY = 0;
    protected static final int DOWNSIZING_MIN_HEAP_CAPACITY = 16;

    protected Comparator<? super K> comparator;

    protected ArrayHandle[] underlyingArray;
    protected int size;
    protected final int minCapacity;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     *
     * <p>
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     * @throws IllegalArgumentException when @{code capacity < 0 || capacity > (Integer.MAX_VALUE - 8 - 1)}
     */
    @ConstantTime()
    @SuppressWarnings("unchecked")
    public AbstractArrayHeap(Comparator<? super K> comparator, int capacity) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        checkCapacity(capacity);
        this.size = 0;
        this.comparator = comparator;
        this.minCapacity = Math.max(capacity, DOWNSIZING_MIN_HEAP_CAPACITY);
        this.underlyingArray = (ArrayHandle[]) Array.newInstance(ArrayHandle.class, minCapacity + 1);
    }

    /**
     * {@inheritDoc}
     * @throws IllegalArgumentException when underlying structure reaches its maximum capacity
     */
    @Override
    @LogarithmicTime(amortized = true)
    public Handle<K, V> insert(K key, V value) {
        if (key == null) {
            throw new NullPointerException("Null keys not permitted");
        }
        if (value == null){
            throw new NullPointerException("Null values not permitted");
        }
        // make sure there is space
        if (size == underlyingArray.length - 1) {
            if (underlyingArray.length == 1) {
                ensureCapacity(1);
            } else {
                ensureCapacity(2 * (underlyingArray.length - 1));
            }
        }

        ArrayHandle p = new ArrayHandle(key, value);
        size++;
        underlyingArray[size] = p;
        p.index = size;

        fixupWithComparator(size);

        return p;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public Option<Handle<K, V>> findMin() {
        if (size == 0) {
            return Option.empty();
        }
        return Option.apply(underlyingArray[1]);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime(amortized = true)
    public Option<Handle<K, V>> deleteMin() {
        if (size == 0) {
            return Option.empty();
        }

        ArrayHandle result = underlyingArray[1];
        result.index = NO_INDEX;
        if (size == 1) {
            underlyingArray[1] = null;
            size = 0;
        } else {
            underlyingArray[1] = underlyingArray[size--];
            fixdownWithComparator(1);
        }
        if (2 * minCapacity < underlyingArray.length - 1 && 4 * size < underlyingArray.length - 1) {
            ensureCapacity((underlyingArray.length - 1) / 2);
        }
        return Option.apply(result);
    }

    @Override
    @LinearTime
    public Iterator<Handle<K, V>> iterator() {
        return new Iterator<Handle<K, V>>() {
            private int i = 0;
            public boolean hasNext() {
                return i < size;
            }

            public Handle<K, V> next() {
                return underlyingArray[++i];
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public long size() {
        return size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public void clear() {
        size = 0;
    }

    protected final void checkCapacity(int capacity) {
        if (capacity < MIN_HEAP_CAPACITY) {
            throw new IllegalArgumentException("Heap capacity must be >= " + MIN_HEAP_CAPACITY);
        }
        if (capacity > MAX_HEAP_CAPACITY) {
            throw new IllegalArgumentException("Heap capacity too large");
        }
    }

    @SuppressWarnings("unchecked")
    private void ensureCapacity(int capacity) {
        checkCapacity(capacity);
        ArrayHandle[] newArray = (ArrayHandle[]) Array.newInstance(ArrayHandle.class, capacity + 1);
        System.arraycopy(underlyingArray, 1, newArray, 1, size);
        underlyingArray = newArray;
    }

    protected abstract void forceFixup(int k);

    protected abstract void fixupWithComparator(int k);

    protected abstract void fixdownWithComparator(int k);

    protected class ArrayHandle implements AddressableHeap.Handle<K, V>, Serializable {
        private final static long serialVersionUID = 1;

        K key;
        V value;
        int index;

        ArrayHandle(K key, V value) {
            this.key = key;
            this.value = value;
            this.index = NO_INDEX;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public void setValue(V value) {
            this.value = value;
        }

        @Override
        @SuppressWarnings("unchecked")
        @LogarithmicTime
        public boolean decreaseKey(K newKey) {
            if (index == NO_INDEX) {
                return false;
            }
            int c = comparator.compare(newKey, key);
            if (c > 0) {
                return false;
            }
            key = newKey;
            if (c == 0 || index == 1) {
                return true;
            }
            fixupWithComparator(index);
            return true;
        }

        @Override
        public boolean delete() {
            if (index == NO_INDEX) {
                return false;
            }

            if (index == 1) {
                deleteMin();
                return true;
            }

            forceFixup(index);
            deleteMin();
            return true;
        }
    }
}