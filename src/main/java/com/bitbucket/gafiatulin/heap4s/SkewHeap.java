package com.bitbucket.gafiatulin.heap4s;

import com.bitbucket.gafiatulin.heap4s.annotations.*;
import scala.Option;

import java.io.Serializable;
import java.util.*;

/**
 * Skew heaps. The heap is sorted by a
 * {@link Comparator} provided at heap creation time.
 *
 * <p>
 * Operations {@code insert}, {@code deleteMin}, and {@code delete} take
 * amortized O(log(n)). Operation {@code findMin} is worst-case O(1). Note that
 * a skew-heap does not efficiently support the operation {@code decreaseKey}
 * which is amortized &#937;(log(n)).
 *
 * {@inheritDoc}
 *
 * @see MergeableHeap
 */
@NotThreadSafe
@TreeBased
public class SkewHeap<K, V> implements MergeableHeap<K, V>, Serializable {
    private final static long serialVersionUID = 1;

    protected final Comparator<? super K> comparator;
    private long size;
    private Node<K, V> root;

    /**
     * Used to reference the current heap or some other heap in case of melding,
     * so that handles remain valid even after a meld, without having to iterate
     * over them.
     *
     * In order to avoid maintaining a full-fledged union-find data structure,
     * we disallow a heap to be used in melding more than once. We use however,
     * path-compression in case of cascading melds, that it, a handle moves from
     * one heap to another and then another.
     */
    protected SkewHeap<K, V> other;

    /**
     * Constructs a new, empty heap, ordered according to the given comparator.
     *
     * <p>
     * All keys inserted into the heap must be <em>mutually comparable</em> by
     * the given comparator: {@code comparator.compare(k1,
     * k2)} must not throw a {@code ClassCastException} for any keys {@code k1}
     * and {@code k2} in the heap. If the user attempts to put a key into the
     * heap that violates this constraint, the {@code insert(Object key)} call
     * will throw a {@code ClassCastException}.
     *
     * @param comparator the comparator that will be used to order this heap.
     *
     * @throws NullPointerException when {@code comparator} is null
     */
    public SkewHeap(Comparator<? super K> comparator) {
        if(comparator == null){
            throw new NullPointerException("Null comparator not permitted");
        }
        this.comparator = comparator;
        this.size = 0;
        this.root = null;
        this.other = this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime(amortized = true)
    @SuppressWarnings("unchecked")
    public AddressableHeap.Handle<K, V> insert(K key, V value) {
        if (key == null) {
            throw new NullPointerException("Null keys not permitted");
        }
        if (value == null){
            throw new NullPointerException("Null values not permitted");
        }
        if (other != this) {
            throw new IllegalStateException("A heap cannot be used after a meld");
        }
        Node<K, V> n = createNode(key, value);

        // easy special cases
        if (size == 0) {
            root = n;
            size = 1;
            return n;
        } else if (size == 1) {
            int c = comparator.compare(key, root.key);
            if (c <= 0) {
                n.oldestChild = root;
                root.parentOrYoungerSibling = n;
                root = n;
            } else {
                root.oldestChild = n;
                n.parentOrYoungerSibling = root;
            }
            size = 2;
            return n;
        }

        root = unionWithComparator(root, n);

        size++;

        return n;
    }


    protected Node<K, V> createNode(K key, V value) {
        return new Node<>(this, key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public Option<AddressableHeap.Handle<K, V>> findMin() {
        if (size == 0) {
            return Option.empty();
        }
        return Option.apply(root);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogarithmicTime(amortized = true)
    public Option<Handle<K, V>> deleteMin() {
        if (size == 0) {
            return Option.empty();
        }
        Node<K, V> oldRoot = root;

        // easy special cases
        if (size == 1) {
            root = null;
            size = 0;
            return Option.apply(oldRoot);
        } else if (size == 2) {
            root = root.oldestChild;
            root.oldestChild = null;
            root.parentOrYoungerSibling = null;
            size = 1;
            oldRoot.oldestChild = null;
            return Option.apply(oldRoot);
        }

        root = unlinkAndUnionChildren(root);
        size--;

        return Option.apply(oldRoot);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LinearTime
    public Iterator<Handle<K, V>> iterator() {
        return new Iterator<Handle<K, V>>() {
            private Node current = root;
            private Deque<Node> stack = new ArrayDeque<>();

            public boolean hasNext() {
                return current != null;
            }

            public Handle<K, V> next() {
                Node previous = current;
                if(current.oldestChild!= null){
                    stack.push(current);
                    current = current.oldestChild;
                } else{
                    if(stack.peek() == current.parentOrYoungerSibling) {
                        stack.pop();
                        current = current.parentOrYoungerSibling.parentOrYoungerSibling;
                        while(stack.peek() == current && !stack.isEmpty()){
                            stack.pop();
                            current = current.parentOrYoungerSibling;
                        }
                    } else {
                        current = current.parentOrYoungerSibling;
                    }
                }
                return previous;
            }
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public long size() {
        return size;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ConstantTime
    public void clear() {
        root = null;
        size = 0;
    }

    @Override
    public void meld(MergeableHeap<K, V> other) {
        SkewHeap<K, V> h = (SkewHeap<K, V>) other;
        // check same comparator
        if (!h.comparator.equals(comparator)) {
            throw new IllegalArgumentException("Cannot meld heaps using different comparators!");
        }

        if (h.other != h) {
            throw new IllegalStateException("A heap cannot be used after a meld.");
        }

        // perform the meld
        size += h.size;
        root = unionWithComparator(root, h.root);

        // clear other
        h.size = 0;
        h.root = null;

        // take ownership
        h.other = this;
    }

    /**
     *  Handle
     */
    static class Node<K, V> implements AddressableHeap.Handle<K, V>, Serializable {

        private final static long serialVersionUID = 1;

        SkewHeap<K, V> heap;

        K key;
        V value;
        Node<K, V> oldestChild; // older child
        Node<K, V> parentOrYoungerSibling; // younger sibling or parent

        Node(SkewHeap<K, V> heap, K key, V value) {
            this.heap = heap;
            this.key = key;
            this.value = value;
            this.oldestChild = null;
            this.parentOrYoungerSibling = null;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public void setValue(V value) {
            this.value = value;
        }

        @Override
        public boolean decreaseKey(K newKey) {
            return getOwner().decreaseKey(this, newKey);
        }

        @Override
        public boolean delete() {
            return getOwner().delete(this);
        }

        /*
         * Get the owner heap of the handle. This is union-find with
         * path-compression between heaps.
         */
        private SkewHeap<K, V> getOwner() {
            if (heap.other != heap) {
                // find root
                SkewHeap<K, V> root = heap;
                while (root != root.other) {
                    root = root.other;
                }
                // path-compression
                SkewHeap<K, V> cur = heap;
                while (cur.other != root) {
                    SkewHeap<K, V> next = cur.other;
                    cur.other = root;
                    cur = next;
                }
                heap = root;
            }
            return heap;
        }
    }

    private boolean decreaseKey(Node<K, V> n, K newKey) {
        int c = comparator.compare(newKey, n.key);

        if (c > 0) {
            return false;
        }
        if (c == 0 || root == n) {
            n.key = newKey;
            return true;
        }

        /*
         * Delete and reinsert
         */
        if(!delete(n)) {
            return false;
        } else {
            n.key = newKey;
            root = unionWithComparator(root, n);
            size++;
            return true;
        }

    }

    protected boolean delete(Node<K, V> n) {
        if (n == root) {
            deleteMin();
            return true;
        }

        if (n.parentOrYoungerSibling == null) {
            return false;
        }

        // disconnect and union children of node
        Node<K, V> childTree = unlinkAndUnionChildren(n);

        // find parent
        Node<K, V> parent = getParent(n);
        if(parent != null) {
            // link children tree in place of node
            if (childTree == null) {
                // no children, just unlink from parent
                if (parent.oldestChild == n) {
                    if (n.parentOrYoungerSibling == parent) {
                        parent.oldestChild = null;
                    } else {
                        parent.oldestChild = n.parentOrYoungerSibling;
                    }
                } else {
                    parent.oldestChild.parentOrYoungerSibling = parent;
                }
            } else {
                // link children tree to parent
                if (parent.oldestChild == n) {
                    childTree.parentOrYoungerSibling = n.parentOrYoungerSibling;
                    parent.oldestChild = childTree;
                } else {
                    parent.oldestChild.parentOrYoungerSibling = childTree;
                    childTree.parentOrYoungerSibling = parent;
                }
            }
        }
        size--;
        n.oldestChild = null;
        n.parentOrYoungerSibling = null;
        return true;
    }

    private Node<K, V> unlinkAndUnionChildren(Node<K, V> n) {
        // disconnect children
        Node<K, V> child1 = n.oldestChild;
        if (child1 == null) {
            return null;
        }
        n.oldestChild = null;

        Node<K, V> child2 = child1.parentOrYoungerSibling;
        if (child2 == n) {
            child2 = null;
        } else {
            child2.parentOrYoungerSibling = null;
        }
        child1.parentOrYoungerSibling = null;

        return unionWithComparator(child1, child2);
    }

    private Node<K, V> getParent(Node<K, V> n) {
        if (n.parentOrYoungerSibling == null) {
            return null;
        }
        Node<K, V> c = n.parentOrYoungerSibling;
        if (c.oldestChild == n) {
            return c;
        }
        Node<K, V> p1 = c.parentOrYoungerSibling;
        if (p1 != null && p1.oldestChild == n) {
            return p1;
        }
        return c;
    }

    protected Node<K, V> unionWithComparator(Node<K, V> root1, Node<K, V> root2) {
        if (root1 == null) {
            return root2;
        } else if (root2 == null) {
            return root1;
        }

        Node<K, V> newRoot;
        Node<K, V> cur;

        // find initial
        int c = comparator.compare(root1.key, root2.key);
        if (c <= 0) {
            newRoot = root1;
            root1 = unlinkRightChild(root1);
        } else {
            newRoot = root2;
            root2 = unlinkRightChild(root2);
        }
        cur = newRoot;

        // merge
        while (root1 != null && root2 != null) {
            c = comparator.compare(root1.key, root2.key);
            if (c <= 0) {
                // link as left child of cur
                if (cur.oldestChild == null) {
                    root1.parentOrYoungerSibling = cur;
                } else {
                    root1.parentOrYoungerSibling = cur.oldestChild;
                }
                cur.oldestChild = root1;
                cur = root1;
                root1 = unlinkRightChild(root1);
            } else {
                // link as left child of cur
                if (cur.oldestChild == null) {
                    root2.parentOrYoungerSibling = cur;
                } else {
                    root2.parentOrYoungerSibling = cur.oldestChild;
                }
                cur.oldestChild = root2;
                cur = root2;
                root2 = unlinkRightChild(root2);
            }
        }

        while (root1 != null) {
            // link as left child of cur
            if (cur.oldestChild == null) {
                root1.parentOrYoungerSibling = cur;
            } else {
                root1.parentOrYoungerSibling = cur.oldestChild;
            }
            cur.oldestChild = root1;
            cur = root1;
            root1 = unlinkRightChild(root1);
        }

        while (root2 != null) {
            // link as left child of cur
            if (cur.oldestChild == null) {
                root2.parentOrYoungerSibling = cur;
            } else {
                root2.parentOrYoungerSibling = cur.oldestChild;
            }
            cur.oldestChild = root2;
            cur = root2;
            root2 = unlinkRightChild(root2);
        }

        return newRoot;
    }

    protected Node<K, V> unlinkRightChild(Node<K, V> n) {
        Node<K, V> left = n.oldestChild;
        if (left == null || left.parentOrYoungerSibling == n) {
            return null;
        }
        //noinspection SuspiciousNameCombination
        Node<K, V> right = left.parentOrYoungerSibling;
        left.parentOrYoungerSibling = n;
        right.parentOrYoungerSibling = null;
        return right;
    }
}
