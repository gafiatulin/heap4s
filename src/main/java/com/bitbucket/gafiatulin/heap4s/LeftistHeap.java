package com.bitbucket.gafiatulin.heap4s;

import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Leftist heaps.
 * <p>
 * Operations {@code insert}, {@code deleteMin}, {@code decreaseKey}, and
 * {@code delete} take worst-case O(log(n)). Operation {@code findMin} is
 * worst-case O(1).
 *
 * {@inheritDoc}
 *
 * @see SkewHeap
 */

public class LeftistHeap<K, V> extends SkewHeap<K, V> {
    private static final long serialVersionUID = -5948402731186806608L;

    public LeftistHeap(Comparator<? super K> comparator) {
        super(comparator);
    }

    static class LeftistNode<K, V> extends Node<K, V> {

        private static final long serialVersionUID = 1L;

        int npl; // null path length

        LeftistNode(LeftistHeap<K, V> heap, K key, V value) {
            super(heap, key, value);
            this.npl = 0;
        }
    }

    @Override
    protected Node<K, V> createNode(K key, V value) {
        return new LeftistNode<>(this, key, value);
    }

    private void swapChildren(Node<K, V> n) {
        Node<K, V> left = n.oldestChild;
        if (left != null) {
            Node<K, V> right = left.parentOrYoungerSibling;
            if (right != n) {
                n.oldestChild = right;
                right.parentOrYoungerSibling = left;
                left.parentOrYoungerSibling = n;
            }
        }
    }

    /**
     * Top-down union of two leftist heaps with comparator.
     *
     * @param root1
     *            the root of the first heap
     * @param root2
     *            the root of the right heap
     * @return the new root of the merged heap
     */
    @Override
    protected Node<K, V> unionWithComparator(Node<K, V> root1, Node<K, V> root2) {
        if (root1 == null) {
            return root2;
        } else if (root2 == null) {
            return root1;
        }

        Node<K, V> newRoot;
        Deque<LeftistNode<K, V>> path = new LinkedList<>();

        // find initial
        int c = comparator.compare(root1.key, root2.key);
        if (c <= 0) {
            newRoot = root1;
            root1 = unlinkRightChild(root1);
        } else {
            newRoot = root2;
            root2 = unlinkRightChild(root2);
        }
        Node<K, V> cur = newRoot;
        path.push((LeftistNode<K, V>) cur);

        // merge
        while (root1 != null && root2 != null) {
            c = comparator.compare(root1.key, root2.key);
            if (c <= 0) {
                // link as right child of cur
                if (cur.oldestChild == null) {
                    cur.oldestChild = root1;
                } else {
                    cur.oldestChild.parentOrYoungerSibling = root1;
                }
                root1.parentOrYoungerSibling = cur;
                cur = root1;
                path.push((LeftistNode<K, V>) cur);
                root1 = unlinkRightChild(root1);
            } else {
                // link as right child of cur
                if (cur.oldestChild == null) {
                    cur.oldestChild = root2;
                } else {
                    cur.oldestChild.parentOrYoungerSibling = root2;
                }
                root2.parentOrYoungerSibling = cur;
                cur = root2;
                path.push((LeftistNode<K, V>) cur);
                root2 = unlinkRightChild(root2);
            }
        }

        if (root1 != null) {
            // link as right child of cur
            if (cur.oldestChild == null) {
                cur.oldestChild = root1;
            } else {
                cur.oldestChild.parentOrYoungerSibling = root1;
            }
            root1.parentOrYoungerSibling = cur;
        }

        if (root2 != null) {
            // link as right child of cur
            if (cur.oldestChild == null) {
                cur.oldestChild = root2;
            } else {
                cur.oldestChild.parentOrYoungerSibling = root2;
            }
            root2.parentOrYoungerSibling = cur;
        }

        /*
         * Traverse path upwards, update null path length and swap if needed.
         */
        while (!path.isEmpty()) {
            LeftistNode<K, V> n = path.pop();

            if (n.oldestChild != null) {
                // at least on child
                LeftistNode<K, V> nLeft = (LeftistNode<K, V>) n.oldestChild;
                int nplLeft = nLeft.npl;
                int nplRight = -1;
                if (nLeft.parentOrYoungerSibling != n) {
                    // two children
                    LeftistNode<K, V> nRight = (LeftistNode<K, V>) nLeft.parentOrYoungerSibling;
                    nplRight = nRight.npl;
                }
                n.npl = 1 + Math.min(nplLeft, nplRight);

                if (nplLeft < nplRight) {
                    // swap
                    swapChildren(n);
                }

            } else {
                // no children
                n.npl = 0;
            }

        }

        return newRoot;
    }

}
