package com.bitbucket.gafiatulin.heap4s;

import scala.Option;

import java.util.Iterator;

/**
 * A heap whose elements can be addressed using handles.
 *
 * An insert operation returns a {@link AddressableHeap.Handle} which can later
 * be used in order to manipulate the element, such as decreasing its key, or
 * deleting it. Storing the handle externally is the responsibility of the user.
 *
 * <p>
 *
 * Note that the ordering maintained by a heap, must be <em>consistent
 * with {@code equals}</em> if this heap is to correctly implement the
 * {@code AddressableHeap} interface. (See {@code Comparator} for a precise
 * definition of <em>consistent with equals</em>.) This is so because the
 * {@code AddressableHeap} interface is defined in terms of the {@code equals}
 * operation, but a heap performs all key comparisons using its {@code compare}
 * method, so two keys that are deemed equal by this method are, from the
 * standpoint of the binary heap, equal. The behavior of a heap <em>is</em>
 * well-defined even if its ordering is inconsistent with {@code equals};
 * it just fails to obey the general contract of the {@code AddressableHeap}
 * interface.
 *
 * @param <K> the type of keys maintained by this heap
 * @param <V> the type of values maintained by this heap
 *
 * @author Dimitrios Michail
 * @author Victor
 */

public interface AddressableHeap<K, V> {
    /**
     * Insert a new element into the heap.
     *
     * @param key the element's key
     * @param value the element's value
     *
     * @return a handle for the newly added element
     *
     * @throws NullPointerException when either {@code key} or {@code value} is null
     */
    AddressableHeap.Handle<K, V> insert(K key, V value);

    /**
     * Find an element with the minimum key.
     *
     * @return optional handle {@code Option<AddressableHeap.Handle<K, V>>} to the element with minimum key (empty if the heap is empty)
     */
    Option<Handle<K, V>> findMin();

    /**
     * Delete and return an element with the minimum key. If multiple such
     * elements exists, only one of them will be deleted. After the element is
     * deleted the handle is invalidated and only method {@link Handle#getKey()}
     * and {@link Handle#getValue()} can be used.
     *
     * @return optional handle {@code Option<AddressableHeap.Handle<K, V>>} to the deleted element with minimum key (empty if the heap is empty)
     */
    Option<AddressableHeap.Handle<K, V>> deleteMin();

    /**
     * Returns the list of {@code Tuple2<K, V>} of elements in the heap.
     */
    Iterator<Handle<K, V>> iterator();

    /**
     * Checks if this heap is empty.
     */
    boolean isEmpty();

    /**
     * Returns the number of elements in the heap.
     */
    long size();

    /**
     * Clear all the elements of the heap. After calling this method all handles
     * should be considered invalidated and the behavior of methods
     * {@link Handle#decreaseKey(Object)} and {@link Handle#delete()} is
     * undefined.
     */
    void clear();

    /**
     * A heap element handle. Allows someone to address an element already in a
     * heap and perform additional operations.
     *
     * @param <K> the type of keys maintained by this heap
     * @param <V> the type of values maintained by this heap
     */
    interface Handle<K, V> {

        /**
         * Returns the key of the element.
         */
        K getKey();

        /**
         * Returns the value of the element.
         */
        V getValue();

        /**
         * Sets the value of the element.
         */
        void setValue(V newValue);

        /**
         * Decreases the key of the element if the new key is smaller than the old key according to the comparator.
         *
         * @param newKey the new key
         * @return {@code true} if successfully decreased key, otherwise — {@code false}
         */
        boolean decreaseKey(K newKey);

        /**
         * Deletes element from the heap it belongs
         * if it hasn't been deleted before
         * by previous call or using {@link AddressableHeap#deleteMin()}.
         *
         * @return {@code true} if successfully deleted element, otherwise — {@code false}
         */
        boolean delete();
    }
}
