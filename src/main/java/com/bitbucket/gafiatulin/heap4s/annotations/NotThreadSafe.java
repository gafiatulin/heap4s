package com.bitbucket.gafiatulin.heap4s.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Marker interface to indicate that Heap implementation is not thread safe.
 *
 * * <strong>!!! This implementation is not synchronized.</strong> If
 * multiple threads access a heap concurrently, and at least one of the threads
 * modifies the heap structurally, it <em>must</em> be synchronized externally.
 * (A structural modification is any operation that adds or deletes one or more
 * elements or changing the key of some element.) This is typically accomplished
 * by synchronizing on some object that naturally encapsulates the heap.
 */
@Target(ElementType.TYPE)
public @interface NotThreadSafe {
}
