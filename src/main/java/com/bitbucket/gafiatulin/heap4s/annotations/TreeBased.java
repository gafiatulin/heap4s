package com.bitbucket.gafiatulin.heap4s.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Marker interface to indicate that Heap implementation is based on Tree structure.
 */
@Target(ElementType.TYPE)
public @interface TreeBased{
}
