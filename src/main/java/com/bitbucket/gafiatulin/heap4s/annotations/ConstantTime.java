package com.bitbucket.gafiatulin.heap4s.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marker interface to indicate that an operation takes constant time. The
 * primary purpose of this interface is to allow generic algorithms to alter
 * their behavior to provide good performance.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.METHOD, ElementType.CONSTRUCTOR })
public @interface ConstantTime {

    /**
     * Whether the running time is amortized or actual.
     *
     * @return {@code true} if amortized, {@code false} if actual
     */
    boolean amortized() default false;

}
